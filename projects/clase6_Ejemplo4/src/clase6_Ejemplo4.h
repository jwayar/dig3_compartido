/*
 * clase6_Ejemplo4.h
 *
 *  Created on: 4 de oct. de 2018
 *      Author: joel
 */

#ifndef CLASE6_EJEMPLO4_H_
#define CLASE6_EJEMPLO4_H_

/*--------------
 * PORT-0
 *--------------*/
#define AddrFIO0DIR  0x2009C000
#define AddrFIO0MASK 0X2009C010
#define AddrFIO0PIN  0x2009C014
#define AddrFIO0SET  0x2009C018
#define AddrFIO0CLR  0x2009C01C

#define AddrPINSEL3     0X4002C00C
#define AddrPCONP       0x400FC0C4
#define AddrPCLKSEL0    0X400FC1A8
#define AddrISER0       0XE000E100
#define AddrT1IR        0x40008000
#define AddrT1TCR       0x40008004
#define AddrT1MCR       0X40008014
#define AddrT1MR0       0X40008018
#define AddrT1EMR       0X4000803C

// PORT.0
unsigned int volatile *const FIO0DIR        = (unsigned int*) AddrFIO0DIR;
unsigned int volatile *const FIO0MASK       = (unsigned int*) AddrFIO0MASK;
unsigned int volatile *const FIO0PIN        = (unsigned int*) AddrFIO0PIN;
unsigned int volatile *const FIO0SET        = (unsigned int*) AddrFIO0SET;
unsigned int volatile *const FIO0CLR        = (unsigned int*) AddrFIO0CLR;

unsigned int volatile *const PINSEL3    = (unsigned int*) AddrPINSEL3   ;
unsigned int volatile *const PCONP      = (unsigned int*) AddrPCONP     ;
unsigned int volatile *const PCLKSEL0   = (unsigned int*) AddrPCLKSEL0  ;
unsigned int volatile *const ISER0      = (unsigned int*) AddrISER0     ;
unsigned int volatile *const T1IR       = (unsigned int*) AddrT1IR      ;
unsigned int volatile *const T1TCR      = (unsigned int*) AddrT1TCR     ;
unsigned int volatile *const T1MCR      = (unsigned int*) AddrT1MCR     ;
unsigned int volatile *const T1MR0      = (unsigned int*) AddrT1MR0     ;
unsigned int volatile *const T1EMR      = (unsigned int*) AddrT1EMR     ;

void configGPIO(void);
void configTimer0(void);
void TIMER0_IRQHandler(void);
#endif /* CLASE6_EJEMPLO4_H_ */
