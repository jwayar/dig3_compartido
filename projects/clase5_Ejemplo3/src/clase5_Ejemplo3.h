/*
===============================================================================
 Name        : clase5_Ejemplo3.h
 Author      : $JWayar
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

/*-------------------------------------------
    Posiciones de memoria para config GPIO0
 *------------------------------------------*/
#define AddrFIO0DIR	0x2009C000 	
#define AddrFIO0SET	0x2009C018 
#define AddrFIO0CLR	0x2009C01C 
#define AddrFIO0PIN	0x2009C014 
/*-------------------------------------------
	Registros Asociados para config SysTick
*------------------------------------------*/
#define AddrSTCTRL  	0xE000E010
#define AddrSTRELOAD 	0XE000E014


unsigned int volatile *const FIO0DIR = (unsigned int*) AddrFIO0DIR;
unsigned int volatile *const FIO0SET = (unsigned int*) AddrFIO0SET;
unsigned int volatile *const FIO0CLR = (unsigned int*) AddrFIO0CLR;
unsigned int volatile *const FIO0PIN = (unsigned int*) AddrFIO0PIN;

unsigned int volatile *const STCTRL   = (unsigned int*) AddrSTCTRL;
unsigned int volatile *const STRELOAD = (unsigned int*) AddrSTRELOAD;

volatile unsigned int SysTickCnt; //Contador de Interrupciones

// Prototipo de las funciones:
void SysTick_Handler (void);
void retardo (unsigned int tick);
