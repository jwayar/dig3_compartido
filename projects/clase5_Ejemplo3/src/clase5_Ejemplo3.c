/*
===============================================================================
 Name        : clase5_Ejemplo3.c
 Author      : $JWayar
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/
#include "clase5_Ejemplo3.h"

void SysTick_Handler(void)
{
	SysTickCnt++;
}

void retardo(unsigned int tick)
{
	SysTickCnt=0;
	*STCTRL |=1;  // Habilita el contador de SysTick
	while(SysTickCnt < tick);
	*STCTRL |=0;  // Deshabilita el contador de SysTick
}

int main(void)
{
	//--- Parameters:
	unsigned int time = 1;  		// Define el tiempo en mseg del sysTick
	unsigned int cclk = 4000000; 	// Frecuencia de funcionamiento del reloj del CPU
	unsigned int tick = 1000 ;
	
	//--- Configuraciones:
	*FIO0DIR  |= (1<<22); 			// P0[22] como salida
	*STCTRL   |= (1<<2); 			// Define la fuente de reloj del SysTick
	*STRELOAD = (cclk/1000)*time-1 ; // Define el valor de recarga del temporizador del SysTick
	*STCTRL  |= (1<<1);    			// Habilita la interrupcion del SysTick

	//--- Programa Principal:
	while(1)
	{
		*FIO0SET |= (1<<22); // P0[22] en alto -> Enciende Led
		retardo(tick);
		*FIO0CLR |= (1<<22); // P0[22] en bajo -> Apaga led
		retardo(tick);
	}
}
