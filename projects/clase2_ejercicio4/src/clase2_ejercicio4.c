/*
===============================================================================
 Name        : clase2_ejercicio4.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

int main(void) {

	int a, *ptr_a;
	ptr_a = &a;
	//¿En la variable de tipo puntero "ptr_a" quedó guardado el valor o la dirección de la variable a?
	// Quedó guardada la dirección de la variable a!

	// La dirección reservada para el entero "a" comienza en 0x10007FD8
	// En la variable ptr_a se almacena la dirección de a, ptr_a=0x10007FD8
	// La dirección de ptr_a no nos interesa, pero se puede ver que se reserva un espacio de memoria
	// para ése puntero, que apunta a un entero
	// En la captura de pantalla de &ptr_a se puede observar la dirección y el contenido de ptr_a

    return 0 ;
}
