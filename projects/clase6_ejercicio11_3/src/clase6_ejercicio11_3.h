/*
 * clase6_ejercicio11_3.h
 *
 *  Created on: 4 de oct. de 2018
 *      Author: joel
 */

#ifndef CLASE6_EJERCICIO11_3_H_
#define CLASE6_EJERCICIO11_3_H_


/*--------------
 * PORT-0
 *--------------*/
#define AddrFIO0DIR 0X2009C000
#define AddrFIO0SET 0X2009C018
#define AddrFIO0CLR 0X2009C01C
#define AddrFIO0PIN 0X2009C014

/*-------------------------------------------
	 Registros Asociados a las
	 Interrupciones por GPIO0 EINT3
*------------------------------------------*/

#define AddrPINSEL0 0X4002C000
#define AddrIO0IntEnR 0X40028090
#define AddrIO0IntEnF 0X40028094
#define AddrIO0IntStatR 0X40028084
#define AddrIO0IntStatF 0X40028088
#define AddrIO0IntClr 0X4002808C

/*-------------------------------------------
	 Registros Asociados a las
	 Interrupciones por TIMER1
*------------------------------------------*/
#define AddrPCONP 0X400FC0C4
#define AddrPCLKSEL0 0X400FC1A8
#define AddrPINSEL3 0X4002C00C
#define AddrT1EMR 0X4000803C
#define AddrT1MCR 0X40008014
#define AddrT1MR0 0X40008018
#define AddrISER0 0XE000E100
#define AddrT1TCR 0X40008004
#define AddrT1IR 0X40008000
#define AddrT1PR 0X4000800C


unsigned int volatile *const FIO0DIR=(unsigned int*)AddrFIO0DIR;
unsigned int volatile *const FIO0SET=(unsigned int*)AddrFIO0SET;
unsigned int volatile *const FIO0CLR=(unsigned int*)AddrFIO0CLR;
unsigned int volatile *const FIO0PIN=(unsigned int*)AddrFIO0PIN;

unsigned int volatile *const PINSEL0= (unsigned int*) AddrPINSEL0;
unsigned int volatile *const IO0IntEnR=(unsigned int*) AddrIO0IntEnR;
unsigned int volatile *const IO0IntEnF= (unsigned int*) AddrIO0IntEnF;
unsigned int volatile *const IO0IntStatR= (unsigned int*) AddrIO0IntStatR;
unsigned int volatile *const IO0IntStatF= (unsigned int*) AddrIO0IntStatF;
unsigned int volatile *const IO0IntClr= (unsigned int*) AddrIO0IntClr;

unsigned int volatile *const PCONP=(unsigned int*)AddrPCONP;
unsigned int volatile *const PCLKSEL0=(unsigned int*)AddrPCLKSEL0;
unsigned int volatile *const PINSEL3=(unsigned int*)AddrPINSEL3;
unsigned int volatile *const T1EMR=(unsigned int*)AddrT1EMR;
unsigned int volatile *const T1MCR=(unsigned int*)AddrT1MCR;
unsigned int volatile *const T1MR0=(unsigned int*)AddrT1MR0;
unsigned int volatile *const ISER0=(unsigned int*)AddrISER0;
unsigned int volatile *const T1TCR=(unsigned int*)AddrT1TCR;
unsigned int volatile *const T1IR=(unsigned int*)AddrT1IR;
unsigned int volatile *const T1PR=(unsigned int*)AddrT1PR;

/*-------------------------------------------
* -- Prototype Functions:
*-------------------------------------------*/
void TIMER1_IRQHandler(void);
void EINT3_IRQHandler(void);

// global variables:


#endif /* CLASE6_EJERCICIO11_3_H_ */
