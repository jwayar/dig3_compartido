/*
===============================================================================
 Name        : clase6_ejercicio11_2.c
 Author      : $JWayar
 Version     :
 Copyright   : $FCEFyN
 Description : main definition
===============================================================================
*/
/* ----------------------------------------------
 * @Output:	P0[22] : Led de la Placa!
 * 			-------------------------------------
 * ----------------------------------------------
 * @Input:	P2[10]  (Pulsador EINT0) pin 51
 * 			-------------------------------------
 *    TIMER0 modo captura
 */

/*
 * Modificar el ejemplo del timer adjunto para que:
 *	2.- La frecuencia de parpadeo disminuya 4 veces por
 *	cada presión de un pulsador debido a la modificación
 *	del pre-escaler. Adjuntar el código en C y un video del funcionamiento.
 *	Nombre del archivo eje11_2ApellidoDelEstudiante.
 */
#include "clase6_ejercicio11_2.h"
#define PULL_UP 1
#define PULL_DOWN !PULL_UP
#define Port2_Pin(x)   x
// Port2_Pin10  // to Interrup
#define Pos_EINT(x)  x
/*--------------------------------------------------
					CONFIGURACION
----------------------------------------------------*/

//-- Config Port
void config_GPIO(void){
    *FIO0DIR |= (1<<22); // led de la placa como salida
	// Input (Set 0):
	*FIO2DIR &= ~(1<<Port2_Pin(10)); // P2[10]
}

//-- Config TIMER0
void configTimer0(void){
    *PCONP    |= (0x1<<1);   // Enciendo el Periferico del Timer
    *PCLKSEL0 |= (0x1<<2);   // Peripheral clock: system clock
    *PINSEL3  |= (0x3<<24); // Pins: Match0.0
    *T0EMR    |= (0x3<<4);  //
    *T0MCR    |= 0x3 ;      // Reset on MR0:
    // the TC will be reset if MR0 matches it
    // Interrupt on MR0: an interrupt is generated 
    // when MR0 matches the value in the TC
    *T0MR0    = 364500 ;     // Match Register 0 (364500)
    *ISER0    |= (0x1<<1);
    *T0TCR    |= 0x1  ;      // Enable Control Register of Timer
    *T0PR=0;
}

//-- Config EINT0
void config_EINT0(void){
	*PINSEL4 	|= (0x01<<20); 	 // "01" para que P2[10] pase a la función1: EINT0

	if(PULL_UP==1)
		*PINMODE4	&= ~ (0x3<<20); // Pull-Up, pero cambiar a "11" para que tenga Pul-Down activada
	else if (PULL_DOWN==1)
		*PINMODE4	|= (0x3<<20); //  "11" para que tenga Pull-Down activada

	*EXTINT 	|= (0x1<<Pos_EINT(0)) ; // "1" para EXINT0, bajo bandera de EINT0

	*EXTMODE    |= (0x1<<Pos_EINT(0)) ; // Config EINT0 por Flanco
	*EXTPOLAR   |= (0x1<<Pos_EINT(0)) ; // Set 1: por Flanco de Subida
}

/*--------------------------------------------------
					INICIALIZACION
----------------------------------------------------*/


/*--------------------------------------------------
					METODOS
----------------------------------------------------*/

// Antirebote:
int debounce(int SampleA){
	//-- Set static variables:
	static int SampleB=0;
	static int SampleC=0;
	static int LastDebounceResult = 0;

	//-- Logical Function:
	LastDebounceResult = (LastDebounceResult &&
						(SampleA || SampleB || SampleC)) ||
						(SampleA && SampleB && SampleC);
	//-- Update Sample
	SampleC=SampleB;
	SampleB=SampleA;

	return LastDebounceResult;
}

/*--------------------------------------------------
					SUBRUTINAS
----------------------------------------------------*/
void TIMER0_IRQHandler(void)
{
    static int i=0;
    if (i==0){
        *FIO0SET |= (1<<22);
        i=!i;
    }
    else if (i==1){
        *FIO0CLR |= (1<<22);
        i=!i;
    }
    *T0IR |=1 ; // MR0 Interrupt Flag Down for Match Channel 0
}

void EINT0_IRQHandler(void){
	//--- Hubo Interrupción por P2.10!
	// Hacer algo -> incrementar T0PR
	static int Puls,PulsAnt;
	//Antirebote
	for(unsigned int j=0; j<3; j++)
	{
		for(int p=0;p<20000;p++); //delay
		if(PULL_UP==1)
			PulsAnt= !(*FIO2PIN & (1<<Port2_Pin(10)));
		else if (PULL_DOWN==1)
			PulsAnt= (*FIO2PIN & (1<<Port2_Pin(10)));
		// en Funcion de la lógica "Pup ó Pdown"
		Puls=debounce(PulsAnt);
	}
	if(Puls==0 && PulsAnt==0)
	{
		// Aumentar T0PR, hasta un valor máximo ()
		*T0PR+=4;
		if(*T0PR > 16){
			*T0PR = 0;
		}
	}

	// Clear Interrupt Flag EINT0
	*EXTINT |= (1<<Pos_EINT(0));
}

/****************************************************
					MAIN
*****************************************************/
int main(void){
	config_GPIO();
	configTimer0();
	config_EINT0();
	//NVIC_EnableIRQ(EINT0_IRQn);
	*ISER0 |= (1<<18); // Set 1 en la Pos 18 del ISER0 -> Enable EINT0
	while(1){
	}
	return 0;
}

