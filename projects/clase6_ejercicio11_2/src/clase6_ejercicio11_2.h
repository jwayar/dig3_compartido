/*
 * clase6_ejercicio11_2.h
 *
 *  Created on: 4 de oct. de 2018
 *      Author: joel
 */

#ifndef CLASE6_EJERCICIO11_2_H_
#define CLASE6_EJERCICIO11_2_H_

/*--------------
 * PORT-0
 *--------------*/
#define AddrFIO0DIR 0X2009C000
#define AddrFIO0SET 0X2009C018
#define AddrFIO0CLR 0X2009C01C
#define AddrFIO0PIN 0X2009C014

/*--------------
 * PORT-2 P2[10] EINT0
 *--------------*/
#define AddrFIO2DIR	 0x2009C040
#define AddrFIO2MASK 0X2009C050
#define AddrFIO2PIN  0X2009C054
#define AddrFIO2SET  0X2009C058
#define AddrFIO2CLR  0X2009C05C
#define AddrPINSEL4  0x4002C010
#define AddrPINMODE4 0x4002C050

/*-------------------------------------------
	 Registros Asociados a las
	 Interrupciones por GPIO2 EINT0,1
*------------------------------------------*/
#define AddrEXTINT 		0x400FC140
#define AddrEXTMODE 	0x400FC148
#define AddrEXTPOLAR	0x400FC14C

/*-------------------------------------------
	 Registros Asociados a las
	 Interrupciones por TIMER0
*------------------------------------------*/
#define AddrPCONP 		0X400FC0C4
#define AddrPCLKSEL0 	0X400FC1A8
#define AddrPINSEL3 	0X4002C00C
#define AddrT0EMR 		0X4000403C
#define AddrT0MCR 		0X40004014
#define AddrT0MR0 		0X40004018
#define AddrT0TCR 		0X40004004
#define AddrT0IR 		0X40004000
#define AddrT0PR 		0X4000400C

/*
 * All Interrupts
 * */
#define AddrISER0 		0XE000E100


// PORT-0
unsigned int volatile *const FIO0DIR     = (unsigned int*) AddrFIO0DIR;
unsigned int volatile *const FIO0SET     = (unsigned int*) AddrFIO0SET;
unsigned int volatile *const FIO0CLR     = (unsigned int*) AddrFIO0CLR;
unsigned int volatile *const FIO0PIN     = (unsigned int*) AddrFIO0PIN;

// PORT-2 P2[10] EINT0
unsigned int volatile *const FIO2DIR 		= (unsigned int*) AddrFIO2DIR;
unsigned int volatile *const FIO2MASK		= (unsigned int*) AddrFIO2MASK;
unsigned int volatile *const FIO2PIN 		= (unsigned int*) AddrFIO2PIN;
unsigned int volatile *const FIO2SET 		= (unsigned int*) AddrFIO2SET;
unsigned int volatile *const FIO2CLR 		= (unsigned int*) AddrFIO2CLR;

unsigned int volatile *const PINSEL4 		= (unsigned int*) AddrPINSEL4;
unsigned int volatile *const PINMODE4       = (unsigned int*) AddrPINMODE4;

// GPIO2 EINT0,1
unsigned int volatile *const EXTINT      = (unsigned int*) AddrEXTINT;
unsigned int volatile *const EXTMODE     = (unsigned int*) AddrEXTMODE;
unsigned int volatile *const EXTPOLAR    = (unsigned int*) AddrEXTPOLAR;

// TIMER0
unsigned int volatile *const PCONP       = (unsigned int*) AddrPCONP;
unsigned int volatile *const PCLKSEL0    = (unsigned int*) AddrPCLKSEL0;
unsigned int volatile *const PINSEL3     = (unsigned int*) AddrPINSEL3;
unsigned int volatile *const T0EMR       = (unsigned int*) AddrT0EMR;
unsigned int volatile *const T0MCR       = (unsigned int*) AddrT0MCR;
unsigned int volatile *const T0MR0       = (unsigned int*) AddrT0MR0;
unsigned int volatile *const ISER0       = (unsigned int*) AddrISER0;
unsigned int volatile *const T0TCR       = (unsigned int*) AddrT0TCR;
unsigned int volatile *const T0IR        = (unsigned int*) AddrT0IR;
unsigned int volatile *const T0PR        = (unsigned int*) AddrT0PR;

/*-------------------------------------------
* -- Prototype Functions:
*-------------------------------------------*/
void config_GPIO(void);
void config_EINT0(void);
void configTimer0(void);
int debounce(int );

void TIMER0_IRQHandler(void);
void EINT0_IRQHandler(void);

// global variables:


#endif /* CLASE6_EJERCICIO11_2_H_ */
