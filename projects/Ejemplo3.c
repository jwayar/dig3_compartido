/* Ejemplo3.c*/

#define AddrPINSEL0 0x4002C000
#define AddrPINSEL1 0x4002C004

#define AddrFIO0DIR	0x2009C000 
#define AddrFIO0SET	0x2009C018 
#define AddrFIO0CLR	0x2009C01C 
#define AddrFIO0PIN	0x2009C014 
/*--------------------------
Registros Asociados a las 
Interrupciones por GPIO0 
--------------------------*/
#define AddrIO0IntEnR 	0x40028090 
#define AddrIO0IntEnF 	0x40028094
#define AddrISER0     	0XE000E100 
#define AddrIO0IntStatR 0x400028084 
#define AddrIO0IntStatF 0x400028088 
#define AddrIO0IntClr 	0x4002808C


unsigned int volatile *const PINSEL0 		= (unsigned int*) AddrPINSEL0;
unsigned int volatile *const PINSEL1 		= (unsigned int*) AddrPINSEL1;
unsigned int volatile *const FIO0DIR 		= (unsigned int*) AddrFIO0DIR;
unsigned int volatile *const FIO0SET 		= (unsigned int*) AddrFIO0SET;
unsigned int volatile *const FIO0CLR 		= (unsigned int*) AddrFIO0CLR;
unsigned int volatile *const FIO0PIN 		= (unsigned int*) AddrFIO0PIN;
unsigned int volatile *const IO0IntEnR 		= (unsigned int*) AddrIO0IntEnR;
unsigned int volatile *const IO0IntEnF 		= (unsigned int*) AddrIO0IntEnF;
unsigned int volatile *const ISER0 			= (unsigned int*) AddrISER0;
unsigned int volatile *const IO0IntStatR 	= (unsigned int*) AddrIO0IntStatR;
unsigned int volatile *const IO0IntStatF 	= (unsigned int*) AddrIO0IntStatF;
unsigned int volatile *const IO0IntClr 		= (unsigned int*) AddrIO0IntClr;

int retardo(unsigned int time){
	unsigned int i;
	for (i=0; i<time; i++); // lazo de demora
	return 0;
}

void EINT3_IRQHandler(void){
	//--- Hubo Interrupción por P0.15 ?
	if( (*IO0IntStatR >>15) & 1){ // Estado de la Interrupcion por Flanco Ascendente para P0.15
		time = 200000;
		*IO0IntClr |= (1<<15);  // Limpio la Interrupción del GPIO para P0.15
	}

	//--- Hubo Interrupción por P0.16 ?
	if( (*IO0IntStatF >> 16) & 1){ // Estado de la Interrupcion por Flanco Descendente para P0.15
		time = 8000;
		*IO0IntClr |= (1<<16); // Limpio
	}

}

int main(void){

	*PINSEL0 &= ~ (3<<30);
	*PINSEL1 &= ~3 & ~(3<<12);
	*FIO0DIR |= (1<<9);  // OutPut P0.9
	*FIO0DIR &= ~ (3<<15);

	*IO0IntEnR |= (1<<15); // Habilito la Interrupción para P0.15 por Flanco de Subida
	*IO0IntEnF |= (1<<16); // Habilito la Interrupción para P0.16 por Flanco de Bajada

	*ISER0 |= (1<<21); // Habilito EINT3

	while(1){

		*FIOSET  |= (1<<9);
		retardo(time);
		*FIO0CLR |= (1<<9);
		retardo(time);
	}
	return 0;
}