/*
===============================================================================
 Name        : clase4_ejercicio7.c
 Author      : $jwayar
 Version     :
 Copyright   : $FCEFyN
 Description : Antirebote
===============================================================================
*/


/* debounce "Eliminador de rebote"
 * @Input: SampleA (la primer muestra de la señal, estado del pin leído por primera vez)
 * @Output LastDebounceResult (Resultado, interpretación del pin leído)
 */
#define PULSADO 1
#define NO_PULSADO 0
int debounce(int SampleA){
	//-- Set static variables:
	static int SampleB=0;
	static int SampleC=0;
	static int LastDebounceResult =0;

	//-- Logical Function:
	LastDebounceResult = (LastDebounceResult &&
						(SampleA || SampleB || SampleC)) ||
						(SampleA && SampleB && SampleC);
	//-- Update Sample
	SampleC=SampleB;
	SampleB=SampleA;

	return LastDebounceResult;
}

int main(void){
	unsigned int boton_test=0;
	unsigned int rebotes=3;	// cantidad de rebotes simuladas
	unsigned int t,j; 		// variables del "for"

	unsigned int status_prev;     // estado del pulsador, anterior al rebote
	unsigned int equal_status;    // el estado del pulsador fue alterado por el rebote?

	//--1 boton_test tomará el estado "PULSADO" ó "NO_PULSADO"
	boton_test=PULSADO;
	status_prev=boton_test; // se guarda el estado previo del pulsador para comparar al final
	boton_test=debounce(boton_test);

	//--2 Aquí simulamos el rebote
	for( j=0; j<rebotes; j++)
	{
		boton_test = ! boton_test;
		for(t=0; t<10; t++){}; // duracion de cada rebote (debido al Hardware, pero se simula en Software)
		boton_test=debounce(boton_test);
	}

	//--3 Pasado el rebote, se estabiliza el pulsador y se toma la ultima muestra
	boton_test=PULSADO;
	boton_test=debounce(boton_test);

	// se mantuvo el estado del pulsador a pesar del rebote?
	if ( boton_test == status_prev)
		equal_status=1;
	else
		equal_status=0;
	// al salir boton_test tendrá el valor del pulsador "sin rebote"

	return 0;
}

