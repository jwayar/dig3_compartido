/*
 * clase7_Ejemplo4.h
 *
 *  Created on: 6 de oct. de 2018
 *      Author: joel
 */

#ifndef CLASE7_EJEMPLO5_H
#define CLASE7_EJEMPLO5_H

//-- To Led-P0[22] as Output
#define AddrFIO0DIR 	0x2009C000
#define AddrFIO0SET 	0x2009C018
#define AddrFIO0CLR 	0x2009C01C

//-- TO ADC P0[26]
#define AddrPCONP 		0x400FC0C4
#define AddrAD0CR 		0x40034000
#define AddrAD0INTEN	0x4003400C
#define AddrPINMODE1 	0x4002C044
#define AddrPINSEL1 	0X4002C004
#define AddrAD0DR0 		0x40034010
#define AddrPCLKSEL0 	0X400FC1A8
#define AddrISER0 		0xE000E100
#define AddrAD0GDR 		0x40034004

unsigned int volatile *const FIO0DIR 	= (unsigned int*) AddrFIO0DIR;
unsigned int volatile *const FIO0SET 	= (unsigned int*) AddrFIO0SET;
unsigned int volatile *const FIO0CLR 	= (unsigned int*) AddrFIO0CLR;

unsigned int volatile *const PCONP 	 	= (unsigned int*) AddrPCONP;
unsigned int volatile *const AD0CR   	= (unsigned int*) AddrAD0CR;
unsigned int volatile *const AD0INTEN 	= (unsigned int*) AddrAD0INTEN;
unsigned int volatile *const PINMODE1 	= (unsigned int*) AddrPINMODE1;
unsigned int volatile *const PINSEL1   = (unsigned int*) AddrPINSEL1;
unsigned int volatile *const AD0DR0 	= (unsigned int*) AddrAD0DR0;
unsigned int volatile *const PCLKSEL0 	= (unsigned int*) AddrPCLKSEL0;
unsigned int volatile *const ISER0 		= (unsigned int*) AddrISER0;
unsigned int volatile *const AD0GDR 	= (unsigned int*) AddrAD0GDR;

//unsigned int volatile *const name_puntero  = (unsigned int*) direccion;

void configADC(void);
void configGPIO(void);


void ADC_IRQHandler(void);


#endif // CLASE7_EJEMPLO5_H
