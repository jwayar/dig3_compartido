/*
===============================================================================
 Name        : clase7_Ejemplo5.c
 Author      : $jwayar
 Version     :
 Copyright   : $ FCEFyN
 Description : main definition
===============================================================================
*/

/* ---------------------------------------------------------
 *  @InPut:
 *	 	PO[23] as AD0[0]
 *
 *	@OutPut:
 *		P0[22]: Led_OFF-ON: de acuerdo a la lectura del conversor, si
 *							supera o no la mitad de la Vref
 *
 * Conexión:
 *	Pin28 V+ ----/\/\/\/\/\---- GND
 *					  |
 *		              |
 *		           P0[23] "AD0.0"
 */

#include "clase7_Ejemplo5.h"

/*--------------------------------------------------
          CONFIGURACION
----------------------------------------------------*/
//-- Config GPIO
void configGPIO(void){
	*FIO0DIR |= (1<<22);
}

//-- Config ADC
void configADC(void){

	/*_1. POWER: ---------------------------------------------
		In the PCONP register (Table 46) */
	*PCONP |= (1<<12); // Power Control for Peripherals Register.
	*AD0CR |= (1<<21); // The A-D converter is operational.

	/*2. Clock: ----------------------------------------------
	In the PCLKSEL0 register (Table 40), select PCLK_ADC. 
	 To scale the clock for the ADC, see bits CLKDIV in Table 532. */
	*PCLKSEL0 |= (3<<24); // "00" -> CCLK/"4"
	*AD0CR &=~ (255<<8); 
		/*
			The APB clock (PCLK_ADC0) is divided by (this value plus one)
			to produce the clock for the A-D converter, 
			wich should be less than or equal to 13MHz.
		*/

	*AD0CR |= (1<<16); // *ADOCR[16] = 1  modo conversiones repetittivas escaneando los pines seleccionados en "SEL"
		/*                   *AD0CR |= (1<<16)
			The AD converter does repeated conversions at up to 200Khz, scanning (if necessary)
			througth the pins selected by bits set to ones in the SEL field.
		*/

	/* 3_ Pins: ------------------------------------------
		Enable ADC0 pins through PINSEL registers.
		Select the pin modes for the port pins with ADC0 functions 
		through the PINMODE registers (Section 8.5).
	*/
	*PINSEL1  |= (1<<14); // PO[23] as AD0[0]
	*PINMODE1 |= (1<<15); // PO[23] Pin has neither PullUp nor PullDown resistor enabled.

	/*	4_ Interrupts: ----------------------------------
		To enable interrupts in te ADC, see Table 536. Interrupts are enabled
		in the NVIC using the appropriate Interrupt Set Enable register.
	*/
	*AD0INTEN = 1 ;
		/* 				>AD0INTEN<
			This register allows control over wich A-D channels generate an interrupt 
			when a conversion complete.

			Completaton of a conversion on ADC channel 0 will not generate an interrupt.
		*/

}

/*--------------------------------------------------
          METODOS
----------------------------------------------------*/
void enableInterrupts(void){

	*ISER0 |= (1<<22); // ADC Interrupt Enable;
}


/*--------------------------------------------------
          SUBRUTINAS
----------------------------------------------------*/
void ADC_IRQHandler(void){
	unsigned short volatile ADC0Value = 0;
	ADC0Value = ( *AD0DR0 >> 4 ) & 0xFFF; // *AD0DR0 contains the most recent conversion

	if ( ADC0Value < 2047)
		*FIO0SET |= (1<<22); // Vadc < Vref/2 -> Prendo Led22
	else
		*FIO0CLR |= (1<<22); // Vadc > Vref/2 -> Apago Led22
}

/****************************************************
          MAIN
*****************************************************/
int main (void){
	// unsigned short volatile ADC0Value=0;

	configADC();
	configGPIO();
	enableInterrupts();
	while(1){
		//ADC0Value = (*AD0DR0 >4) & 0xFFF;
		//if(ADC0Value<2047)
		//	*FIO0SET |= (1<<22);
		//else
		//	*FIO0CLR |= (1<<22);
	}

	return 0;
}
