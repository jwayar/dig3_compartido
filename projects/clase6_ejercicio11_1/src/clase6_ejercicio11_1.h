/*
 * clase6_ejercicio11_1.h
 *
 *  Created on: 4 de oct. de 2018
 *      Author: joel
 */

#ifndef CLASE6_EJERCICIO11_1_H_
#define CLASE6_EJERCICIO11_1_H_

/*--------------
 * PORT-0
 *--------------*/
#define AddrFIO0DIR  0x2009C000
#define AddrFIO0MASK 0X2009C010
#define AddrFIO0PIN  0x2009C014
#define AddrFIO0SET  0x2009C018
#define AddrFIO0CLR  0x2009C01C

#define AddrPINSEL3     0X4002C00C
#define AddrPCONP       0x400FC0C4
#define AddrPCLKSEL0    0X400FC1A8
#define AddrISER0       0XE000E100
#define AddrT0IR        0x40004000
#define AddrT0TCR       0x40004004
#define AddrT0MCR       0X40004014
#define AddrT0MR0       0X40004018
#define AddrT0EMR       0X4000403C

// PORT.0
unsigned int volatile *const FIO0DIR        = (unsigned int*) AddrFIO0DIR;
unsigned int volatile *const FIO0MASK       = (unsigned int*) AddrFIO0MASK;
unsigned int volatile *const FIO0PIN        = (unsigned int*) AddrFIO0PIN;
unsigned int volatile *const FIO0SET        = (unsigned int*) AddrFIO0SET;
unsigned int volatile *const FIO0CLR        = (unsigned int*) AddrFIO0CLR;

unsigned int volatile *const PINSEL3    = (unsigned int*) AddrPINSEL3   ;
unsigned int volatile *const PCONP      = (unsigned int*) AddrPCONP     ;
unsigned int volatile *const PCLKSEL0   = (unsigned int*) AddrPCLKSEL0  ;
unsigned int volatile *const ISER0      = (unsigned int*) AddrISER0     ;
unsigned int volatile *const T0IR       = (unsigned int*) AddrT0IR      ;
unsigned int volatile *const T0TCR      = (unsigned int*) AddrT0TCR     ;
unsigned int volatile *const T0MCR      = (unsigned int*) AddrT0MCR     ;
unsigned int volatile *const T0MR0      = (unsigned int*) AddrT0MR0     ;
unsigned int volatile *const T0EMR      = (unsigned int*) AddrT0EMR     ;

void configGPIO(void);
void configTimer0(void);
void TIMER0_IRQHandler(void);
#endif /* CLASE6_EJERCICIO11_1_H_ */
