#define AddrFIO0DIR 0X2009C000  //Define las posiciones de memoria
#define AddrFIO0SET 0X2009C018  // donde se encuentran los registros
#define AddrFIO0CLR 0X2009C01C  // que configurar al GPIO0
#define AddrFIO0PIN 0X2009C014

/* Definir Puertos a las direcciones
 * de memoria por las correspondientes constantes:
 */
unsigned int volatile *const FIO0DIR = (unsigned int*) AddrFIO0DIR;
unsigned int volatile *const FIO0SET = (unsigned int*) AddrFIO0SET;
unsigned int volatile *const FIO0CLR = (unsigned int*) AddrFIO0CLR;
unsigned int volatile *const FIO0PIN = (unsigned int*) AddrFIO0PIN;

// función retardo
int retardo(unsigned int time){
	unsigned int i;
	for (i=0; i<time; i++); // lazo de demora
	return 0;
}

int main(void){
	// Configuración:
	unsigned int time  = 364500 ; // control de tiempo de retardo

	*FIO0DIR |= (1<<22); 		// define el pin22 del P0 como salida

	// Acción principal:

	while(1){
		*FIO0SET |= (1<<22);    // P0[22] en alto
		retardo(time);
		*FIO0CLR |= (1<<22); 	// P0[22] en bajo
		retardo(time);
	}
	return 0;
}
