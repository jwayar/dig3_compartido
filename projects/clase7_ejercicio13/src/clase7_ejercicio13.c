/*
===============================================================================
 Name        : clase7_ejercicio13.c
 Author      : $ Jwayar
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/


/* ---------------------------------------------------------
 *  @InPut:
 *	 	PO[23] as AD0[0]
 *      P2[10]  (Pulsador EINT0) pin 51
 *	@OutPut:
 *		P0[22]: Led_OFF-ON: de acuerdo a la lectura del conversor, si
 *							supera o no la mitad de la Vref
 *
 * Conexión:
 *	Pin28 V+ ----/\/\/\/\/\---- GND
 *					  |
 *		              |
 *		           P0[23] "AD0.0"
 */

/*
 * Modificar el ejemplo del ADC para que
 * el inicio de conversión sea controlado por el pin P2.10 EINT0
 * "Se debe configurar el ADC con Burst=0" -> Software controlled mode
 * [Bit 24:26] – START
 * When the BURST [16 bit] is 0, these bits control whether and when an A/D conversion is started:
 *  000 - Conversion Stopped
 *  001- Start Conversion Now
 * */

#include "clase7_ejercicio13.h"

#define PULL_UP 1
#define PULL_DOWN !PULL_UP
#define Port2_Pin(x)   x
// Port2_Pin10  // to Interrup
#define Pos_EINT(x)  x
#define VREF 		5
/*--------------------------------------------------
          CONFIGURACION
----------------------------------------------------*/
//-- Config GPIO
void configGPIO(void){
	*FIO0DIR |= (1<<22);
}

//-- Config EINT0
void config_EINT0(void){
	*PINSEL4 	|= (0x01<<20); 	 // "01" para que P2[10] pase a la función1: EINT0

	if(PULL_UP==1)
		*PINMODE4	&= ~ (0x3<<20); // Pull-Up, activada
	else if (PULL_DOWN==1)
		*PINMODE4	|= (0x3<<20); //   Pull-Down activada

	*EXTINT 	|= (0x1<<Pos_EINT(0)) ; // "1" para EXINT0, bajo bandera de EINT0

	*EXTMODE    |= (0x1<<Pos_EINT(0)) ; // Config EINT0 por Flanco
	*EXTPOLAR   |= (0x1<<Pos_EINT(0)) ; // Set 1: por Flanco de Subida
}

//-- Config ADC Mode Burst
void configADC(void){

	/*_1. POWER: ---------------------------------------------
		In the PCONP register (Table 46) */
	*PCONP |= (1<<12); // Power Control for Peripherals Register.
	*AD0CR |= (1<<21); // The A-D converter is operational.

	/*2. Clock: ----------------------------------------------
	In the PCLKSEL0 register (Table 40), select PCLK_ADC.
	 To scale the clock for the ADC, see bits CLKDIV in Table 532. */
	*PCLKSEL0 |= (3<<24); // "00" -> CCLK/"4"
	*AD0CR &=~ (255<<8); // CLK_DIV = 255
		/*
			The APB clock (PCLK_ADC0) is divided by (this value plus one)
			to produce the clock for the A-D converter,
			wich should be less than or equal to 13MHz.
		*/

	*AD0CR &=~ (1<<16); // *ADOCR[16] = 0 -> BURST=0
		/*
			Software controlled mode:
			para iniciar conversion -> *AD0CR |= (1<<24)
		*/

	/* 3_ Pins: ------------------------------------------
		Enable ADC0 pins through PINSEL registers.
		Select the pin modes for the port pins with ADC0 functions
		through the PINMODE registers (Section 8.5).
	*/
	*PINSEL1  |= (1<<14); // PO[23] as AD0[0]
	*PINMODE1 |= (1<<15); // PO[23] Pin has neither PullUp nor PullDown resistor enabled.
	*AD0CR    |= (1<<0);        /* Select Channel 0 by setting 0th bit of ADCR */
	/*	4_ Interrupts: ----------------------------------
		To enable interrupts in te ADC, see Table 536. Interrupts are enabled
		in the NVIC using the appropriate Interrupt Set Enable register.
	*/
	*AD0INTEN = 1 ;
		/* 				>AD0INTEN<
			This register allows control over wich A-D channels generate an interrupt
			when a conversion complete.

			Completaton of a conversion on ADC channel 0 will not generate an interrupt.
		*/

}

/*--------------------------------------------------
          METODOS
----------------------------------------------------*/
void enableInterrupts(void){
	*ISER0 |= (1<<22); // ADC Interrupt Enable;
	*ISER0 |= (1<<18); // Set 1 en la Pos 18 del ISER0 -> Enable EINT0
}

// Antirebote:
int debounce(int SampleA){
  //-- Set static variables:
  static int SampleB=0;
  static int SampleC=0;
  static int LastDebounceResult = 0;

  //-- Logical Function:
  LastDebounceResult = (LastDebounceResult &&
            (SampleA || SampleB || SampleC)) ||
            (SampleA && SampleB && SampleC);
  //-- Update Sample
  SampleC=SampleB;
  SampleB=SampleA;

  return LastDebounceResult;
}


/*--------------------------------------------------
          SUBRUTINAS
----------------------------------------------------*/
void EINT0_IRQHandler(void){
	//*FIO0SET |= (1<<22); // para probar que ingrese por EINT0
	//--- Hubo Interrupción por P2.10!
	// Hacer algo -> incrementar T0PR
	static int Puls,PulsAnt;
	//Antirebote
	for(unsigned int j=0; j<3; j++)
	{
		for(int p=0;p<20000;p++); //delay
		if(PULL_UP==1)
			PulsAnt= !(*FIO2PIN & (1<<Port2_Pin(10)));
		else if (PULL_DOWN==1)
			PulsAnt= (*FIO2PIN & (1<<Port2_Pin(10)));
		// en Funcion de la lógica "Pup ó Pdown"
		Puls=debounce(PulsAnt);
	}
	if(Puls==0 && PulsAnt==0)
	{
		//*FIO0SET |= (1<<22);
		//for(int j=0;j<60000;j++);
		//*FIO0CLR |= (1<<22);
		//for(int j=0;j<60000;j++);
		// Iniciar conversion del ADC
		*AD0CR |= (1<<24);
	}

	// Clear Interrupt Flag EINT0
	*EXTINT |= (1<<Pos_EINT(0));
}

void ADC_IRQHandler(void){
	unsigned short volatile ADC0Value = 0;
	//float volts = 0;
	ADC0Value = ( *AD0DR0 >> 4 ) & 0xFFF; // *AD0DR0 contains the most recent conversion

	if ( ADC0Value < 2047)
		*FIO0SET |= (1<<22); // Vadc < Vref/2 -> Prendo Led22
	else
		*FIO0CLR |= (1<<22); // Vadc > Vref/2 -> Apago Led22

	volts = (ADC0Value*VREF)/4096.0; //Convert result to Voltage
	//printf("AD0.0 = %dmV\n" , (int)(volts*1000)); //Display milli-volts
}

/****************************************************
          MAIN
*****************************************************/
int main (void){
	// unsigned short volatile ADC0Value=0;

	configADC();
	configGPIO();
	config_EINT0();
	enableInterrupts();
	while(1){
		//ADC0Value = (*AD0DR0 >4) & 0xFFF;
		//if(ADC0Value<2047)
		//	*FIO0SET |= (1<<22);
		//else
		//	*FIO0CLR |= (1<<22);
	}

	return 0;
}
