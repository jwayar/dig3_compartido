/*
===============================================================================
 Name        : DAC_rampa_Sanchez.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

//-- Config TMR0
void config_TMR0( void ){
	LPC_SC->PCON |= (1<<1); // Habilito T0 desde el Source Control
	LPC_TIM0->CTCR = 0; 	 // Timer0 en modo Temporizador

	LPC_TIM0->PC   = 25000 ;  // cada que PC cuente 25mil TC incrementa en 1
	//LPC_TIM0->PR   = 0 ;  // Preescaler, cuenta hasta éste valor para incrementar TMRO contador

	//-- Match:
	LPC_TIM0->MR0  = 250000; // Este valor no se resetea automaticamente
	LPC_TIM0->MR1  = 500000; // El valor mayor es el que debe resetearse en la config

	LPC_TIM0->MCR |= (1<<0);  // Interrupt on MR0[0]
	LPC_TIM0->MCR &= ~(1<<1); // No reset on MR0[0]
	LPC_TIM0->MCR |= (1<<3);  // Interrupt on MR0[1]
	LPC_TIM0->MCR |= (1<<4);  // Reset on MR0[1]

	//--Interrupciones en NVIC
	LPC_TIM0->TCR |= (1<<0); // Contador MR0[0] Habilitado
	LPC_TIM0->TCR |= (1<<1); // Contador MR0[1] Habilitado
	LPC_TIM0->TCR &=~ (1<<1); // Reseteamos el Contador

	LPC_TIM0->IR  &=~ (3<<0); // Bajamos la bandera de MR0[0] y MR0[1]
}

//-- Config NVIC
void config_NVIC(void){
	NVIC_EnableIRQ(TIMER0_IRQn);
}
void config_DAC(void){

	//-- Config Clock:
	LPC_SC->PCLKSEL0 &= ~(1<<22);
	LPC_SC->PCLKSEL0 &= ~(1<<23); // PCLKdac = 100/4 = 25Mhz

	//-- Config Pin:
	LPC_PINCON->PINSEL1 &= ~(1<<20); //
	LPC_PINCON->PINSEL1 |= (1<<21);  // P0[26] Function '10'= AOUT, Salida del DAC
	//LPC_PINCON->PINMODE1 &=~ (1<<20);
	//LPC_PINCON->PINMODE1 |= (1<<21); // P0[26] Pin Mode '10'= Sin Pull-UP/Down

}


//-- TIMER0 IRQHandler
void TIMER0_IRQHandler(void){
	static int DAC_Value=0;

	LPC_DAC->DACR = (DAC_Value & 0x3FF) << 6 ;
	// Leo los 10 bits (0x3FF), lo cargo en el DAC (pin 6 en adelante)
	// el dato se convierte y sale por el Pin de Salida de manera automatica
	DAC_Value++; // La escencia de una rampa estilo Diente de Sierra

	if( LPC_TIM0->IR & (1<<0) ){
		LPC_TIM0->IR |= (1<<0); 	// Bajo Bandera MR0[0]
	}else if ( LPC_TIM0->IR & (1<<1) ){
		LPC_TIM0->IR |= (1<<1); 	// Bajo Bandera MR0[1]
	}
}

/****************************************
 *------ MAIN
****************************************/
int main(void) {
	config_TMR0();
	config_DAC();
	config_NVIC();
    while(1) {
    }
    return 0 ;
}
