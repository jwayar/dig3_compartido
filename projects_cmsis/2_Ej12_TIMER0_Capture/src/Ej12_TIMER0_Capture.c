/*
===============================================================================
 Name        : Ej12_TIMER0_Capture.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>


//LED
#define LED     (1 << 22)
#define LED_ON     LPC_GPIO0->FIOSET=LED
#define LED_OFF    LPC_GPIO0->FIOCLR=LED
#define LED_TOG    LPC_GPIO0->FIOPIN^=LED

void TIMER0_IRQHandler(void)
{
 uint32_t reg_val;
 reg_val = LPC_TIM0->IR;
 if(reg_val & (1<<4))            //CR0 interrupt
 {
  if(LPC_GPIO1->FIOPIN & (1<<26))//high?
  {
   LED_ON;
  }
  else
  {
   LED_OFF;
  }
  LPC_TIM0->IR = (1<<4);        //reset interrupt
 }
}

int main(void)
{
 LPC_GPIO0->FIODIR |= LED;        //LED output
 volatile static int i = 0 ;
//setup timer 0 capture
//Setup P1.26 as CAP0.0
 LPC_PINCON->PINSEL3 |= (3<<20);    //set capture 0.0
//Note: reset values of timer registers are 0, so setting them isn't necessary
 LPC_TIM0->CCR =((1<<0)|(1<<1)|(1<<2));        //capture rising & falling with interrupt
 LPC_TIM0->TCR = 1;                //start timer
 NVIC_EnableIRQ(TIMER0_IRQn);
 while(1)
 {
  i++ ;
 }
 return 0 ;
}
