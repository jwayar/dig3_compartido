/*
===============================================================================
 Name        : 5_adc_TP.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

/*
 * Conexión1:
 *	Pin28 V+ ----/\/\/\/\/\---- GND
 *					  |
 *		              |
 *		           P0[23] "AD0.0"(in)
 *
 * Conexión2:
 *	Pin28 V+ ----/\/\/\/\/\---- GND
 *					  |
 *		              |
 *		           P0[24] (in) "AD0.1"
 */

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

/*--------------------------------------------------
          CONFIGURACION
----------------------------------------------------*/
//-- Config GPIO
void config_GPIO(void){
	LPC_GPIO0->FIODIR |= (1<<22);
}

//-- Config ADC
void config_ADC(void){

	/*_1. POWER: ---------------------------------------------
		In the PCONP register (Table 46) */
	LPC_SC->PCONP |= (1<<12); // Power Control for Peripherals Register.
	LPC_ADC->ADCR |= (1<<21); // The A-D converter is operational.

	/*2. Clock: ----------------------------------------------
	In the PCLKSEL0 register (Table 40), select PCLK_ADC.
	 To scale the clock for the ADC, see bits CLKDIV in Table 532. */
	LPC_SC->PCLKSEL0 |= (3<<24); // "00" -> CCLK/"4"
	LPC_ADC->ADCR &=~ (255<<8);
		/*
			The APB clock (PCLK_ADC0) is divided by (this value plus one)
			to produce the clock for the A-D converter,
			wich should be less than or equal to 13MHz.
		*/

	LPC_ADC->ADCR |= (1<<16); // *ADOCR[16] = 1  modo conversiones repetittivas escaneando los pines seleccionados en "SEL"
		/*                   *AD0CR |= (1<<16)
			The AD converter does repeated conversions at up to 200Khz, scanning (if necessary)
			througth the pins selected by bits set to ones in the SEL field.
		*/

	/* 3_ Pins: ------------------------------------------
		Enable ADC0 pins through PINSEL registers.
		Select the pin modes for the port pins with ADC0 functions
		through the PINMODE registers (Section 8.5).
	*/
	LPC_PINCON->PINSEL1  |= (1<<14); // PO[23] as AD0[0]
	LPC_PINCON->PINMODE1 |= (1<<15); // PO[23] Pin has neither PullUp nor PullDown resistor enabled.
	LPC_PINCON->PINSEL1  |= (1<<16); // PO[24] as AD0[0]
	LPC_PINCON->PINMODE1 |= (1<<17); // PO[24] Pin has neither PullUp nor PullDown resistor enabled.

	LPC_ADC->ADCR |= (1<<0); // AD0.0 seleccionado
	LPC_ADC->ADCR |= (1<<1); // AD0.1 seleccionado

	/*	4_ Interrupts: ----------------------------------
		To enable interrupts in te ADC, see Table 536. Interrupts are enabled
		in the NVIC using the appropriate Interrupt Set Enable register.
	*/
	LPC_ADC->ADINTEN = 3 ;
		/* 				>AD0INTEN<
			This register allows control over wich A-D channels generate an interrupt
			when a conversion complete.

			Completaton of a conversion on ADC channel 0 will not generate an interrupt.
		*/
}


/*--------------------------------------------------
          METODOS
----------------------------------------------------*/
void enableInterrupts(void){
	NVIC_EnableIRQ(ADC_IRQn); // ADC Interrupt Enable;
}

/*--------------------------------------------------
          SUBRUTINAS
----------------------------------------------------*/
void ADC_IRQHandler(void){
	unsigned short volatile ADC0Value0 = 0;
	unsigned short volatile ADC0Value1 = 0;

	if ((LPC_ADC->ADSTAT&1)==1) // AD0.0 is Done!
	{
		ADC0Value0=(LPC_ADC->ADDR0 >>4)&0xFFF; // *AD0DR0 contains the most recent conversion

		if(ADC0Value0<2047)
			LPC_GPIO0->FIOSET |=(1<<22); // Vadc < Vref/2 -> Prendo Led22
		else
			LPC_GPIO0->FIOCLR |=(1<<22); // Vadc > Vref/2 -> Apago Led22
	}
	if(((LPC_ADC->ADSTAT>>1)&1)==1) // AD0.1 is Done!
	{
		ADC0Value1=(LPC_ADC->ADDR1 >>4)&0xFFF; // *AD0DR0 contains the most recent conversion

	}

}

/****************************************************
          MAIN
*****************************************************/
int main (void){

	config_ADC();
	config_GPIO();
	enableInterrupts();
	while(1){
	}

	return 0;
}

