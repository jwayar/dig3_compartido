/*
===============================================================================
 Name        : pwm2_TP.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#define PRESCALE (25000-1) //25000 PCLK clock cycles to increment TC by 1

#include <cr_section_macros.h>

void config_TIMER0(void)
{
	/*Assuming that PLL0 has been setup with CCLK = 100Mhz and PCLK = 25Mhz.*/
	LPC_SC->PCONP |= (1<<1); //Power up TIM0. By default TIM0 and TIM1 are enabled.
	LPC_SC->PCLKSEL0 &= ~(0x3<<3); //Set PCLK for timer = CCLK/4 = 100/4 (default)

	LPC_TIM0->CTCR = 0x0;
	LPC_TIM0->PR = PRESCALE; //Increment LPC_TIM0->TC at every 24999+1 clock cycles
	//25000 clock cycles @25Mhz = 1 mS

	LPC_TIM0->MR0 = 500; //Toggle Time in mS
	LPC_TIM0->MCR |= (1<<0) | (1<<1); // Interrupt & Reset on MR0 match
	LPC_TIM0->TCR |= (1<<1); //Reset Timer0

	NVIC_EnableIRQ(TIMER0_IRQn); //Enable timer interrupt

	LPC_TIM0->TCR = 0x01; //Enable timer
}

void TIMER0_IRQHandler(void){
	volatile static int i = 0 ;
	if(i==0)
		LPC_GPIO0->FIOSET |= (1<<22);
	else
		LPC_GPIO0->FIOCLR |= (1<<22);
	i++;
	i=i%2;
	LPC_TIM0->IR |= (1<<0); //Clear MR0 Interrupt flag
}

int main(void) {

	config_TIMER0();
	LPC_GPIO0->FIODIR |= (1<<22);

    while(1) {
    }
    return 0 ;
}
