/*
===============================================================================
 Name        : DAC_rampa.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

void	SystemInit( ) { }

void	delay( ) {
	int i = 10000;
	while(i)
	{
		i--;
	}
}

void	Vout(int v) {
	LPC_DAC->DACR = v <<6;
}

int	main ( ) {
	int v;
	LPC_PINCON->PINSEL1 |= 0x02<<20;
	LPC_SC->PCLKSEL0 |= 1 <<24;
	while(1){
		Vout(v++);
		v &= 0x3FF;
		delay();   //will allow signal to be observed on a multimeter
	}
}
