
/*
* Create by Jwayar
* 11/10/2018
*/

//-- Config UART0
config_UART0(void){
	/* 1) Habilitar el Periférico: */
	LPC_SC -> PCONP |=(1<<3);

	/* 2) Configurar PCLK_UART */
	LPC_SC-> PCLKEL0 &= ~ (3<<6) // PCLK_UART0 = 25 MHz

	/* 3) Trama de Datos Baud Rate */
	LPC_UART0-> LCR |= (3<<0) | (1<<7) ;
		/*
		[1:0]: 8 bit character length
		[7]  : Enable acces to Divisor Latches
		[2]  : "0" with 1 stop bit
		[3]	 : "0" Disable parity generation and cheking
		[5:4]: "00" para config tipo de paridad, pero está desabilitada!
		[6]  : "0" Disable break transmission
		*/
	/* 4) Config Baud Rate to BR=9600 (deseado) */
	LPC_UART0 -> DLM= 0;
	LPC_UART0 -> DLL= 163;
		// BaudRate= PCLK / (16 * (256*DLM + DLL)) = 9585,89
		// error= 0,156% < 1,1 %
		//   entonces, no es necesario el Divisor Fraccionario

	/* 5) Deshabilito LSB p/acceder a RBR, THR, IER */
	LPC_UART0 -> LCR &= ~ (1<<7);

	/* 6) Config Pines Tx-Rx*/
		// P0[2] as Tx
		// P0[3] as Rx
	LPC_PINCON ->PINSEL0 |= (1<<4) | (1<<6) ;

	/* 7) Habilitar UART0 */
	LPC_UARTO0 ->IER |= (1<<0);
		// Habrá interrupcion cuando un dato recibido esté disponible!

	LPC_UARTO0 ->IER |=(1<<1);
		// Habrá interrupciones cuando el THR está vacío 
		//   y se puede enviar otro byte

	//-- Enable Interrupción:
	NVIC_Enable_IRQ(UARTO0_IRQn);

}

/*
* Create by Jwayar
* 11/10/2018
*/