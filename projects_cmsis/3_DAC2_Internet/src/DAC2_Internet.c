/*
===============================================================================
 Name        : main.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

short cos1[ ] = {1023,1022,1016,1006,993,976,954,931,904,874,841,806,768,728,
                 687,645,601,557,512,468,423,379,337,296,256,219,183,150,120,
                 93,69,48,31,18,8,2,0,2,8,18,31,48,69,93,120,150,183,219,256,
                 296,337,379,423,468,512,557,601,645,687,728,768,806,841,
                 874,904,931,954,976,993,1006,1016,1022,1023};

void SystemInit( ) {  }   //expected by start up code

void Vout(short v) {
	LPC_DAC->DACR = v <<6;
}

void SysTick_Handler() {	//non maskable interrupt routine
	static int v;
	Vout( cos1[v]);
	v++;
	if (cos1[v] == 1023) v=0;
}

void initSysTick()		{
       SysTick->LOAD = 0x800;
       SysTick->VAL  = 800;
       SysTick->CTRL = 7;        //Enable SysTick, interrupts using sys clock
}

int main ( ) {
	LPC_PINCON->PINSEL1 |= 0x02<<20;
	//LPC_SC->PCLKSEL0 |= 1 <<22;
	LPC_SC->PCLKSEL0 |= 1 <<24;
	initSysTick();
	 while(1)
	 {
		  //other actions
	 }
}
