/*
===============================================================================
 Name        : UART2_joel.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/
/*
===============================================================================
 Name        : UART_joel.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/
		// P0[2] as Tx Pin 21
		// P0[3] as Rx Pin 22

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

/*
* Create by Jwayar
* 11/10/2018
*/

//-- Config UART2
void config_UART2(void){
	/* 1) Habilitar el Periférico: */
	LPC_SC -> PCONP |=(1<<24);

	/* 2) Configurar PCLK_UART */
	LPC_SC-> PCLKSEL1 &= ~ (3<<16); // PCLK_UART0 = CCLK/4 = 25 MHz

	/* 3) Trama de Datos Baud Rate */
	LPC_UART2-> LCR |= (3<<0) ; // 8bit
		/*
		[1:0]: 8 bit character length
		[7]  : Enable acces to Divisor Latches
		[2]  : "0" with 1 stop bit
		[3]	 : "0" Disable parity generation and cheking
		[5:4]: "00" para config tipo de paridad, pero está desabilitada!
		[6]  : "0" Disable break transmission
		*/
	/* 4) Config Baud Rate to BR=9600 (deseado) */
	LPC_UART2-> LCR |=  (1<<7) ; // habilito LSB p/acceder a RBR, THR, IER

	LPC_UART2 -> DLM= 0x00;
	LPC_UART2 -> DLL= 0xa3 ;// 163 en decimal;
		// BaudRate= PCLK / (16 * (256*DLM + DLL)) = 9585,89
		// error= 0,156% < 1,1 %
		//   entonces, no es necesario el Divisor Fraccionario

	/* 5) Deshabilito LSB p/acceder a RBR, THR, IER */
	LPC_UART0 -> LCR &= ~ (1<<7);

	/* 6) Config Pines Tx-Rx*/
		// P0[10] as Tx2
		// P0[11] as Rx2
	LPC_PINCON ->PINSEL0 |= (5<<20) ;

	/* 7) Habilitar UART0 */
	LPC_UART2 ->IER |= (1<<0);
		// Habrá interrupcion cuando un dato recibido esté disponible!

	//LPC_UART2 ->IER |=(1<<1);
		// Habrá interrupciones cuando el THR está vacío
		//   y se puede enviar otro byte
}

//-- enviar
void enviar(char c){
	//unsigned int frecuencia = SystemCoreClock;
	while( (LPC_UART2->LSR & (1<<5) ) == 0); // chequea si THR contiene un dato valido ó está vacío
	LPC_UART2->THR = c;
}

//-- Interrupcion
void UART2_IRQHandler(void){
	//unsigned int frec = SystemCoreClock;

	char k;
	int i;
	k= LPC_UART2->RBR;
	char c[]= "hola mundo \r\n";
	c[11]=k;
	for(i=0; c[i];i++) // transmitir un string predefinido
	{
		enviar(c[i]);
	}
}


int main(void){
	//unsigned int a = SystemCoreClock;
	//LPC_SC-> PCLKSEL0 = 0x0 ;
	config_UART2();

	//-- Enable Interrupción:
	NVIC_EnableIRQ(UART2_IRQn);

	while(1);
}


/*
* Create by Jwayar
* 11/10/2018
*/
