#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>


void config_UART(void);


int main(void){

	config_UART();

	while(1){
	}
	return 0;
}

void config_UART(void){
	LPC_SC -> PCONP|=(1<<24); 		//UART 2 power/clock control bit
	LPC_SC -> PCLKSEL1&=~(3<<16);	//Peripheral clock selection for UART2:CCLK/4 "default"

	LPC_PINCON -> PINSEL0|=(5<<20);		//Configure P0.10 as Tx and P0.11 as Rx

	LPC_UART2 -> LCR|=3;				//Word length select:8-BIT character length, stop bit select:1stop bit,
	//LPC_UART2 -> LCR|=(1<<3);
	//LPC_UART2 -> LCR|=(1<<4);
	//LPC_UART2 -> LCR&=~(1<<5);
	//Parity Enable: Even parity
	LPC_UART2 -> LCR|=(1<<7);			//enable access to divisor latches
	LPC_UART2 -> DLL=163;				//14; The UARTn Divisor Latch LSB Register, along with the UnDLM
	LPC_UART2 -> DLM=0;				//register, determines the baud rate of the UARTn
	LPC_UART2 -> LCR&=~(1<<7); 		//Disable access to Divisor Latches
	LPC_UART2 -> IER|=5;				//Enables the REcieve Data Available interrupt for UARTn
	NVIC_EnableIRQ(UART2_IRQn);		//UART2 Interrupt Enable
}

//-- Interrupcion
void UART2_IRQHandler(void){
	char k;
	int i;
	k= LPC_UART2->RBR;
	char c[]= "hola mundo \r\n";
	c[11]=k;
	for(i=0; c[i];i++) // transmitir un string predefinido
	{
		enviar(c[i]);
	}
}

void enviar(char c){
	while((LPC_UART2->LSR&(1<<5))==0);//check if UnTHR contains valid data or is empty
	LPC_UART2->THR=c;
}
