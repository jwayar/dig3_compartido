/*
===============================================================================
 Name        : DAC.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

/*  Pin H[18] de la Placa
 *  AD3  P0.[26] 0-GPIO, 1-AD0[3], 2-AOUT, 3-RXD3 20,21 bits of PINSEL1
 *
 * */

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

//-- DEFINE
#define NUM_SAMPLE_SINE         60
//-- COSNTANTES
uint32_t sin_0_to_90_16_samples[16]={\
                         	 	 	 0,1045,2079,3090,4067,\
									 5000,5877,6691,7431,8090,\
									 8660,9135,9510,9781,9945,10000\
									};
uint32_t dac_lut[NUM_SAMPLE_SINE];
//-- Data to DAC:
void data_To_Dac(void){
	for( int i=0;i<NUM_SAMPLE_SINE;i++)
	{
		if(i<=15)
		{
			dac_lut[i] = 512 + 512*sin_0_to_90_16_samples[i]/10000;
			if(i==15) dac_lut[i]= 1023;
		}
		else if(i<=30)
		{
			dac_lut[i] = 512 + 512*sin_0_to_90_16_samples[30-i]/10000;
		}
		else if(i<=45)
		{
			dac_lut[i] = 512 - 512*sin_0_to_90_16_samples[i-30]/10000;
		}
		else
		{
			dac_lut[i] = 512 - 512*sin_0_to_90_16_samples[60-i]/10000;
		}
		dac_lut[i] = (dac_lut[i]<<6);
	}
}

//-- Config_DAC
void config_dac(void){

	//-- SELECTING CLOCK FOR DAC "PCLKSEL0"
	LPC_SC->PCLKSEL0 	|= 1 <<22; // pclk = cclk

	//-- SELECTING THE DAC FUNCTION TO GPIO "PINSEL1"
	LPC_PINCON->PINSEL1 |= 0x02<<20; //Pin P0.26 allocated to alternate function 2
	LPC_PINCON->PINMODE1 |= 0x02<<20;      // Pull up/down on Pin P0.26 disabled

	//-- DAC Registers and DAC Operation "DACR" (15:6 "VALUE")

}

/*********************************************************
 *---------------  MAIN
 *********************************************************/
int main(void) {

    // TODO: insert code here

    // Force the counter to be placed into memory
    volatile static int i = 0 ;
    // Enter an infinite loop, just incrementing a counter
    while(1) {
        i++ ;
    }
    return 0 ;
}
