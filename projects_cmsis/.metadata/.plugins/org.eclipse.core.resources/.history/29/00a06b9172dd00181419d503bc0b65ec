/*
===============================================================================
 Name        : pwm_TP.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#define PWMPRESCALE (25-1) //25 PCLK cycles to increment TC by 1 i.e. 1 Micro-second

void config_PWM1(void);
void config_Timer0(void);

int main(void)
{
	//SystemInit(); //gets called by Startup code before main()
	config_Timer0(); //Initialize Timer
	config_PWM1(); //Initialize PWM

	while(1)
	{
	}
  //return 0; //normally this won't execute ever
}

void config_PWM1(void)
{
	/*Assuming that PLL0 has been setup with CCLK = 100Mhz and PCLK = 25Mhz.*/

	LPC_PINCON->PINSEL3 |= (1<<5); //Select PWM1.1 output for Pin1.18
	LPC_PWM1->PCR = 0x0; //Select Single Edge PWM - by default its single Edged so this line can be removed
	LPC_PWM1->PR = PWMPRESCALE; //1 micro-second resolution
	LPC_PWM1->MR0 = 20000; //20000us = 20ms period duration
	LPC_PWM1->MR1 = 1250; //1ms - default pulse duration i.e. width
	LPC_PWM1->MCR = (1<<1); //Reset PWM TC on PWM1MR0 match
	LPC_PWM1->LER = (1<<1) | (1<<0); //update values in MR0 and MR1
	LPC_PWM1->PCR = (1<<9); //enable PWM output
	LPC_PWM1->TCR = (1<<1); //Reset PWM TC & PR

	LPC_PWM1->TCR = (1<<0) | (1<<3); //enable counters and PWM Mode

	//PWM Generation goes active now!
	//Now you can get the PWM output on Pin P1.18
}

void config_Timer0(void) //To setup Timer0 used delayMS() function
{
	/*Assuming that PLL0 has been setup with CCLK = 100Mhz and PCLK = 25Mhz.*/
	LPC_SC->PCONP |= (1<<1);
	LPC_TIM0->CTCR = 0x0;
	LPC_TIM0->PR = 25000-1; //Increment TC at every 24999+1 clock cycles
	//25000 clock cycles @25Mhz = 1 mS

	LPC_TIM0->MCR |= (3<<0); // Interrupt + Reset

	NVIC_EnableIRQ(TIMER0_IRQn);
	LPC_TIM0->TCR = 0x01; //Enable timer

}

void TIMER0_IRQHanlder(void)
{
	int pulseWidths[] = {50,250,450,650,850,1050,1250,1450,1650,1850,2000};
	const int numPulseWidths = 11;
	static int count=1;

	//Update Servo Pulse Width
	LPC_PWM1->MR1 = pulseWidths[count]; //Update MR1 with new value
	LPC_PWM1->LER = (1<<1); //Load the MR1 new value at start of next cycle

	if(count == (numPulseWidths-1) ){
		count=0;
	}
	else
		count++;

	LPC_TIM0->IR |= (1<<0);
}
