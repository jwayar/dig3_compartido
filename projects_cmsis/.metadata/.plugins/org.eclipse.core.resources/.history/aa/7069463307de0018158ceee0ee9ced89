/*
===============================================================================
 Name        : pwm2_TP.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#define TIM0PRESCALE (25000-1) // "miliSeg" 25000 PCLK clock cycles to increment TC by 1
#define PWMPRESCALE (25-1)     // "microSeg" 25 PCLK cycles to increment TC by 1 i.e. 1 Micro-second
#define PWMANGULOS    7 	   // cantidad de ángulos
#define MR0VALUE 	1000 	   // 500 mSeg
#define MR1VALUE 	MR0VALUE*PWMANGULOS //

#include <cr_section_macros.h>

void config_TIMER0(void)
{
	/*Assuming that PLL0 has been setup with CCLK = 100Mhz and PCLK = 25Mhz.*/
	LPC_SC->PCONP |= (1<<1); //Power up TIM0. By default TIM0 and TIM1 are enabled.
	LPC_SC->PCLKSEL0 &= ~(0x3<<3); //Set PCLK for timer = CCLK/4 = 100/4 (default)

	LPC_TIM0->CTCR = 0x0;
	LPC_TIM0->PR = TIM0PRESCALE; //Increment LPC_TIM0->TC at every 24999+1 clock cycles
	//25000 clock cycles @25Mhz = 1 mS

	LPC_TIM0->MR0 = MR0VALUE; //Toggle Time in mS
	//LPC_TIM1->MR1 = MR1VALUE;

	LPC_TIM0->MCR |= (3<<0); // Interrupt & Reset TC on MR0 match
	//LPC_TIM0->MCR |= (3<<3); // Interrupt & Reset TC on MR0 match

	LPC_TIM0->TCR |= (1<<1); //Reset Timer0

	NVIC_EnableIRQ(TIMER0_IRQn); //Enable timer interrupt

	LPC_TIM0->TCR = 0x01; //Enable timer
}

void config_PWM1(void)
{
	/*Assuming that PLL0 has been setup with CCLK = 100Mhz and PCLK = 25Mhz.*/

	LPC_PINCON->PINSEL3 |= (1<<5); //Select PWM1.1 output for Pin1.18
	LPC_PWM1->PCR = 0x0; //Select Single Edge PWM - by default its single Edged so this line can be removed
	LPC_PWM1->PR = PWMPRESCALE; //1 micro-second resolution
	LPC_PWM1->MR0 = 20000; //20000us = 20ms period duration
	LPC_PWM1->MR1 = 1250; //1ms - default pulse duration i.e. width
	LPC_PWM1->MCR = (1<<1); //Reset PWM TC on PWM1MR0 match
	LPC_PWM1->LER = (1<<1) | (1<<0); //update values in MR0 and MR1
	LPC_PWM1->PCR = (1<<9); //enable PWM output
	LPC_PWM1->TCR = (1<<1); //Reset PWM TC & PR

	LPC_PWM1->TCR = (1<<0) | (1<<3); //enable counters and PWM Mode

	//PWM Generation goes active now!
	//Now you can get the PWM output on Pin P1.18
}

void TIMER0_IRQHandler(void){

	//-- SERVO CONTROL:
	volatile static int pulseWidths = 50; // first pulse

	//Update Servo Pulse Width
	LPC_PWM1->MR1 = pulseWidths; //Update MR1 with new value (<20mil)
	LPC_PWM1->LER = (1<<1); //Load the MR1 new value at start of next cycle

	if(pulseWidths>= 2000 ){
		pulseWidths=250;
		LPC_GPIO0->FIOCLR |= (1<<22);
		//LPC_TIM0->MR0 = 1500;
		LPC_TIM0->PR = TIM0PRESCALE*5;
	}
	else
	{
		pulseWidths+=250;
		LPC_GPIO0->FIOSET |= (1<<22);
		//LPC_TIM0->MR0 = 500;
		LPC_TIM0->PR = TIM0PRESCALE;
	}

	LPC_TIM0->IR |= (1<<0); //Clear MR0 Interrupt flag
}

int main(void) {

	config_TIMER0();
	config_PWM1();
	LPC_GPIO0->FIODIR |= (1<<22);

    while(1) {
    }
    return 0 ;
}
