//EJEMPLO del timer 0 con interrupciones y cambiando el prescaler
//Concepcion Alvarado Ian Erik
#define AddrFIO0DIR 0X2009C000
#define AddrFIO0SET 0X2009C018
#define AddrFIO0CLR 0X2009C01C
#define AddrFIO0PIN 0X2009C014
#define AddrPINMODE0 0X2009C040


#define AddrPINSEL0 0X4002C000
#define AddrIO0IntEnR 0X40028090 //registros asociados a las
#define AddrIO0IntEnF 0X40028094 // interrupciones por GPIO
#define AddrIO0IntStatR 0X40028084
#define AddrIO0IntStatF 0X40028088
#define AddrIO0IntClr 0X4002808C

#define AddrPCONP 0X400FC0C4
#define AddrPCLKSEL0 0X400FC1A8
#define AddrPINSEL3 0X4002C00C
#define AddrT1EMR 0X4000803C
#define AddrT1MCR 0X40008014
#define AddrT1MR0 0X40008018
#define AddrISER0 0XE000E100
#define AddrT1TCR 0X40008004
#define AddrT1IR 0X40008000
#define AddrT1PR 0X4000800C

unsigned int volatile *const FIO0DIR=(unsigned int*)AddrFIO0DIR;
unsigned int volatile *const FIO0SET=(unsigned int*)AddrFIO0SET;
unsigned int volatile *const FIO0CLR=(unsigned int*)AddrFIO0CLR;
unsigned int volatile *const FIO0PIN=(unsigned int*)AddrFIO0PIN;

unsigned int volatile *const PINSEL0= (unsigned int*) AddrPINSEL0;
unsigned int volatile *const IO0IntEnR=(unsigned int*) AddrIO0IntEnR;
unsigned int volatile *const IO0IntEnF= (unsigned int*) AddrIO0IntEnF;
unsigned int volatile *const IO0IntStatR= (unsigned int*) AddrIO0IntStatR;
unsigned int volatile *const IO0IntStatF= (unsigned int*) AddrIO0IntStatF;
unsigned int volatile *const IO0IntClr= (unsigned int*) AddrIO0IntClr;

unsigned int volatile *const PCONP=(unsigned int*)AddrPCONP;
unsigned int volatile *const PCLKSEL0=(unsigned int*)AddrPCLKSEL0;
unsigned int volatile *const PINSEL3=(unsigned int*)AddrPINSEL3;
unsigned int volatile *const T1EMR=(unsigned int*)AddrT1EMR;
unsigned int volatile *const T1MCR=(unsigned int*)AddrT1MCR;
unsigned int volatile *const T1MR0=(unsigned int*)AddrT1MR0;
unsigned int volatile *const ISER0=(unsigned int*)AddrISER0;
unsigned int volatile *const T1TCR=(unsigned int*)AddrT1TCR;
unsigned int volatile *const T1IR=(unsigned int*)AddrT1IR;
unsigned int volatile *const T1PR=(unsigned int*)AddrT1PR;
unsigned int volatile *const PINMODE0 = (unsigned int*) AddrPINMODE0;

void TIMER1_IRQHandler(void);
void EINT3_IRQHandler(void);


int main(void){
	*PINSEL0 &=~(3<<30);
	*FIO0DIR &=~(3<<15);
	//*PINMODE0 &= ~(0x03<<15);
	//*PINMODE0 &= ~(0x03<<16);

	*IO0IntEnF|=(1<<15); //enable falling edge interrupt for P.0.15
	*IO0IntEnF|=(1<<16); //enable falling edge interrupt for P.0.16


	*PCONP|=(1<<2);		//enciendo el periferico del timer
	*PCLKSEL0|=(1<<4);	//peripheral clock: system clock
	*PINSEL3|=(3<<12);	//pins: match0.0
	*T1EMR|=(3<<4);
	*T1MCR|=3;			//reset on MR0 the TC will be reset if MR0 matches it. interrup on MR0: an interrup is
						//generated when MR0 matches the value in the TC
	*T1MR0=364500;		//match register 0
	*ISER0|=(1<<2);
	*T1TCR|=1;			//habilito el registro del control del timer

	*ISER0|=(1<<21); //external interrupt 3 interrupt enable

	*FIO0DIR|=(1<<22);
	while(1){
	}
	return 0;
}

void TIMER1_IRQHandler(void){
	static int i=0;
	if(i==0){
		*FIO0SET|=(1<<22);
		i=1;
	}
	else if(i==1){
		*FIO0CLR|=(1<<22);
		i=0;
	}
	*T1IR|=1;				//MR0 interrupt flag for match channel 0
}

void EINT3_IRQHandler(void){
	if((*IO0IntStatF>>15)&1){//  EJECUTA LA INTERRUPCION  EN EL PIN 15
		*T1PR+=1;//Registro de PR Prescale. Cuando el contador de preescala  PC (a continuaci�n) es igual a
                  //este valor, el siguiente reloj incrementa el TC y borra la PC.
                  //PC Prescale Counter. La PC de 32 bits es un contador que se incrementa
               //al valor almacenado en PR. Cuando se alcanza el valor en PR, la TC se incrementa y la PC se borra. La PC es observable y
                //controlable a trav�s de la interfaz de bus
                //Contador de temporizador TC. La TC de 32 bits se incrementa cada PR + 1 ciclo de
                //PCLK. El TC se controla a trav�s del TCR.TODO ESTO ES LOS REGISTROS DEL TIMER/COUNTER
		}
	if((*IO0IntStatF>>16)&1){ //status of rising edge interrupt for P.0.15,PARA EL PIN 16
		*T1PR=0;
		 //clear GPIO port interrups for P.0.16
	}
	*IO0IntClr|=(1<<15);
	*IO0IntClr|=(1<<16);
}
