#define AddrFIO0DIR 0x2009C000   //Define las posiciones de memoria
#define AddrFIO0SET 0x2009C018   // donde se encuentran los registros
#define AddrFIO0CLR 0x2009C01C	 // que configuran al GPIO0
#define AddrFIO0PIN 0x2009C014

unsigned int volatile *const FIO0DIR=(unsigned int*)AddrFIO0DIR; // Define los punteros a las direcciones
unsigned int volatile *const FIO0SET=(unsigned int*)AddrFIO0SET; // de memoria definidas por las
unsigned int volatile *const FIO0CLR=(unsigned int*)AddrFIO0CLR; // correspondientes constantes.
unsigned int volatile *const FIO0PIN=(unsigned int*)AddrFIO0PIN;

int retardo(unsigned int); // prototipo de la funcion retardo

int main(void) {
	float a= -3.30927268281464709298589355058E-14;
	int d,b;
	unsigned int *e;
	e=&a;
	int c=0;
	unsigned int time =364500; //control de tiempo de retardo
	*FIO0DIR|=(1<<22);  //define al pin 22 del puerto 0 como salida (en "1")

        while(1) {
        	b=31-c;
        	d=(*e&(1<<b))&&1;
        	*FIO0SET |=(1<<22); //pone en alto al pin 22, encendiendo el led
        	retardo(time);
        	*FIO0CLR |=(d<<22); // pone en cero al pin 22, apagando el led
        	retardo(time);
        	c++;
        	if(c==32){
        		c=0;
        	}
    }
    return 0 ;
}

int retardo(unsigned int time){
unsigned int i;
for(i=0;i<time;i++); //lazo de demora
return 0;
}

