#define AddrFIO0DIR 0x2009C000 //Define las posiciones de memorias
#define AddrFIO0SET 0x2009C018 //donde se encuentran los registros
#define AddrFIO0CLR 0x2009C01C // que configuran al GPIO0

#define AddrPCONP 0x400FC0C4
#define AddrAD0CR 0x40034000
#define AddrAD0INTEN 0x4003400C
#define AddrPINMODE1 0x4002C044
#define AddrPINSEL1 0x4002C004
#define AddrAD0DR0 0x40034010
#define AddrPCLKSEL0 0x400FC1A8
#define AddrISER0 0xE000E100  //pagina 79
//#define AddrAD0GDR 0x40034004

unsigned int volatile *const FIO0DIR=(unsigned int*)AddrFIO0DIR;
unsigned int volatile *const FIO0SET=(unsigned int*)AddrFIO0SET;
unsigned int volatile *const FIO0CLR=(unsigned int*)AddrFIO0CLR;

unsigned int volatile *const PCONP=(unsigned int*)AddrPCONP;
unsigned int volatile *const AD0CR=(unsigned int*)AddrAD0CR;
unsigned int volatile *const AD0INTEN=(unsigned int*)AddrAD0INTEN;
unsigned int volatile *const PINMODE1=(unsigned int*)AddrPINMODE1;
unsigned int volatile *const PINSEL1=(unsigned int*)AddrPINSEL1;
unsigned int volatile *const AD0DR0=(unsigned int*)AddrAD0DR0;
unsigned int volatile *const PCLKSEL0=(unsigned int*)AddrPCLKSEL0;
unsigned int volatile *const ISER0=(unsigned int*)AddrISER0;
//unsigned int volatile *const AD0GDR=(unsigned int*)AddrAD0GDR;

void adcConfig(void);
void ADC_IRQHandler(void);

int main(void){
//	unsigned short volatile ADC0Value=0;
	adcConfig();
	*FIO0DIR|=(1<<22);

	while(1){
//		ADC0Value=(*AD0DR0>>4)&0xFFF;
//		if(ADC0Value<2047)
//			*FIO0SET|=(1<<22);
//		else
//			*FIO0CLR|=(1<<22);
	}
	return 0;
}

void adcConfig(){
//1.Power: In the PCONP register (Table46), set the PCADC bit.
	*PCONP|=(1<<12); //Power Control for Peripherals Register.
	*AD0CR|=(1<<21);//the A/D converter is operational.
//2.Clock: In the PCLKSEL0 register (Table 40), select PCLK_ADC. To scale the clock for the ADC, see bits CLKDIV in Table 532.
	*PCLKSEL0|=(3<<24);
	*AD0CR&=~(255<<8); /*The APB clock (PCLK_ADC0) is divided by (this value plus one) to produce the clock for
						the A/D converter, which should be less than or equal to 13MHz.*/
	*AD0CR|=(1<<16); /*The AD converter does repeated conversions at up to 200Khz, scanning (if necessary)
						through the pins selected by bits set to ones in the SEL field.*/
//3.Pins: Enable ADCo pins through PINSEL registers.Select the pin modes for the port pins with ADC0 functions through the PINMODE registers (Section 8.5).
	*PINMODE1|=(1<<15);
	*PINSEL1|=(1<<14);
//4.Interrupts: To enable interrupts in the ADC, see Table 536. Interrupts are enabled in the NVIC using the appropriate Interrupt Set Enable register.
	*AD0INTEN=1; //This register allows control over which A/D channels generate an interrupt when a conversion is complete.
	//Completion of a conversion on ADC channel 0 will not generate an interrupt.
	*ISER0|=(1<<22); //ADC Interrupt Enable.
}

void ADC_IRQHandler(void){
	unsigned short volatile ADC0Value=0;
	ADC0Value=(*AD0DR0>>4)&0xFFF;
	if(ADC0Value<2047)
		*FIO0SET|=(1<<22);
	else
		*FIO0CLR|=(1<<22);
}
