#define AddrFIO0DIR 0X2009C000
#define AddrFIO0SET 0X2009C018
#define AddrFIO0CLR 0X2009C01C
#define AddrFIO0PIN 0X2009C014

//----------GPIO_INTERRUPT-----------
#define AddrPINSEL0 0X4002C000
#define AddrIO0IntEnR 0X40028090 //registros asociados a las
#define AddrIO0IntEnF 0X40028094 // interrupciones por GPIO
#define AddrIO0IntStatR 0X40028084
#define AddrIO0IntStatF 0X40028088
#define AddrIO0IntClr 0X4002808C

#define AddrPCONP 0X400FC0C4
#define AddrPCLKSEL1 0x400FC1AC
#define AddrPINSEL3 0X4002C00C
#define AddrISER0 0XE000E100

//----------TIMER2-------------
#define AddrT2EMR 0X4009003C
#define AddrT2MCR 0X40090014
#define AddrT2MR0 0X40090018
#define AddrT2TCR 0X40090004
#define AddrT2IR 0X40090000
#define AddrT2PR 0X4009000C

unsigned int volatile *const FIO0DIR=(unsigned int*)AddrFIO0DIR;
unsigned int volatile *const FIO0SET=(unsigned int*)AddrFIO0SET;
unsigned int volatile *const FIO0CLR=(unsigned int*)AddrFIO0CLR;
unsigned int volatile *const FIO0PIN=(unsigned int*)AddrFIO0PIN;

unsigned int volatile *const PINSEL0= (unsigned int*) AddrPINSEL0;
unsigned int volatile *const IO0IntEnR=(unsigned int*) AddrIO0IntEnR;
unsigned int volatile *const IO0IntEnF= (unsigned int*) AddrIO0IntEnF;
unsigned int volatile *const IO0IntStatR= (unsigned int*) AddrIO0IntStatR;
unsigned int volatile *const IO0IntStatF= (unsigned int*) AddrIO0IntStatF;
unsigned int volatile *const IO0IntClr= (unsigned int*) AddrIO0IntClr;

unsigned int volatile *const PCONP=(unsigned int*)AddrPCONP;
unsigned int volatile *const PCLKSEL1=(unsigned int*)AddrPCLKSEL1;
unsigned int volatile *const PINSEL3=(unsigned int*)AddrPINSEL3;
unsigned int volatile *const ISER0=(unsigned int*)AddrISER0;

unsigned int volatile *const T2EMR=(unsigned int*)AddrT2EMR;
unsigned int volatile *const T2MCR=(unsigned int*)AddrT2MCR;
unsigned int volatile *const T2MR0=(unsigned int*)AddrT2MR0;
unsigned int volatile *const T2TCR=(unsigned int*)AddrT2TCR;
unsigned int volatile *const T2IR=(unsigned int*)AddrT2IR;
unsigned int volatile *const T2PR=(unsigned int*)AddrT2PR;

void paso(void);
void direc(int);
void stop(void);
void start(void);
int antirebote(int);
int retardo(unsigned int);
int direccion=0;
int Puls,PulsAnt;
int pasos=0;
int bandera=0;
int angulo;
int main(void){
	*PINSEL0 &=~(3<<30);  //configura interrupciones GPIO
	*FIO0DIR &=~(3<<15);
	*IO0IntEnF|=(1<<15); //enable falling edge interrupt for P.0.15
	*IO0IntEnF|=(1<<16); //enable falling edge interrupt for P.0.16

	*FIO0DIR|=(15<<6);	//P0.8 y P0.9 Salidas
	*FIO0CLR|=(15<<6);  //salidas en 0

	*PCONP|=(1<<22); //TIMER2 ON
	*PCLKSEL1|=(1<<12);	//TIMER2 CLOCK=CCLK
	*T2MCR|=3;			//interrupcion timer habilitada

	*T2MR0=100000;
	*ISER0|=(1<<3);
	*T2TCR|=1;

	*ISER0|=(1<<21);
	stop();			//para el motor
	start();
	while(1){

	}
	return 0;
}

void TIMER2_IRQHandler(void){
	paso();					//Genera un ciclo de determinados pasos
	*T2IR|=1;
	*IO0IntClr|=(1<<15);
	angulo=75*pasos;
	angulo=angulo/10;
}

void paso(void){
	static int contador=0;
		if (direccion==0){			// dependiendo de la direccion genera la secuencia en un sentido o en el otro
			pasos++;
			contador++;
			if(contador==4)
				contador=0;
		}
		if (direccion==1){
			pasos--;
			contador--;
			if(contador<0)
				contador=3;
		}
	switch (contador){					//P0.6=In1 P0.7=In2 P0.8=In3  P0.9=In4
		case 0:
			*FIO0SET|=(5<<6);		//	  | paso1 | paso2 | paso3 | paso4 |
			*FIO0CLR|=(5<<7);		//In1 |	  1	  |	  1	  |	  0	  |	  0	  |
			break;					//In2 |	  0	  |	  0	  |	  1	  |	  1	  |
		case 1:						//In3 |	  1	  |	  0	  |   0	  |   1   |
			*FIO0SET|=(9<<6);		//In4 |	  0	  |	  1	  |   1	  |	  0   |
			*FIO0CLR|=(3<<7);
			break;
		case 2:
			*FIO0SET|=(5<<7);
			*FIO0CLR|=(5<<6);
			break;
		case 3:
			*FIO0SET|=(3<<7);
			*FIO0CLR|=(9<<6);
			break;
	}
}

void EINT3_IRQHandler(void){
	if((*IO0IntStatF>>15)&1){		//P0.15 Angulo 0
		retardo(5000);
		Puls=(*FIO0PIN>>15)&1;
		PulsAnt=Puls;
		Puls=antirebote(Puls);
		retardo(10000);
		if(Puls==0&& PulsAnt==0){
			stop();
			if (bandera==0){
				direc(0);
				bandera=1;
				pasos=0;
			}
			else{
				direc(1);
				bandera=0;
			}
			start();
		}
	}
	if((*IO0IntStatF>>16)&1){			//gira el motor una cantidad determinada de pasos
		retardo(1000);
		Puls=(*FIO0PIN>>16)&1;
			PulsAnt=Puls;
			Puls=antirebote(Puls);
			retardo(5000);
			if(Puls==0&& PulsAnt==0){//status of rising edge interrupt for P.0.15
				pasos=28;
				start();
			}
	}
	//*IO0IntClr|=(1<<15);
	*IO0IntClr|=(1<<16);
}

void start(void){			//enciende el motor
	*T2MCR|=1;
	*T2TCR&=~(1<<1);
}

void stop(void){			//apaga el motor
	*FIO0CLR|=(15<<6);
	*T2MCR&=~1;
	*T2TCR|=(1<<1);
}

void direc(int a){			//cambia la direccion del motor
	if (a==1)
		direccion=1;
	else
		direccion=0;
}

int antirebote(int SampleA)			//antirebote para flanco de bajada
{
static int SampleB = 0;
static int SampleC = 0;
static int UltimoResultado = 0;

UltimoResultado = (UltimoResultado & (SampleA | SampleB | SampleC)) | (SampleA & SampleB & SampleC);
SampleC = SampleB;
SampleB = SampleA;
return UltimoResultado;
}

int retardo(unsigned int time){
unsigned int i;
for(i=0;i<time;i++); //lazo de demora
return 0;
}
