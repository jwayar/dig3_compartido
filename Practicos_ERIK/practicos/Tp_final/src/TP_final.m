%-----------------------------------+
%           Serial Communication     +
%-----------------------------------+
clear all;close all;clc;
%----------dibujar_escala----------
figure('units','normalized','outerposition',[0 0 1 1]);
th = linspace(0,2*pi,100);
R = 10:5:40;  
for i=1:length(R);
x = R(i)*cos(th);
y = R(i)*sin(th);
plot(x,y,'b','LineWidth',1);
grid on;
hold on;
end
%--------Dibujar_Angulos----------
%espaciado cada 30 grados
x0 = [0 -40 -34.64 -20 20 34.64 ];  x1 = [0 40 34.64 20 -20 -34.64]; y0 = [-40 0 -20 -34.64 -34.64 -20]; y1 = [40 0 20 34.64 34.64 20];

for i=1:length(x0);
hold on;
plot([x0(i),x1(i)],[y0(i),y1(i)] ,'b','LineWidth',2);
end
%--------ComunicationConfig-------
delete(instrfind({'Port'},{'COM3'}));
buff=4;
s=serial('COM3');
set(s,'BaudRate',9600);
set(s,'Terminator','');
set(s,'InputBufferSize',buff);
fopen(s);
%-----------plot_data---------------
while(1)
    dato=fread(s);%Read binary data from device
    radio=dato(2)*256+dato(1);
    angulo=dato(4)*256+dato(3);
%---------grad_a_rad----------
    angulo=angulo*2*pi/360;
%---------pol2cart----------------------
    x=radio*cos(angulo);
    y=radio*sin(angulo);
%--------muestra_valores----------------
    display(radio);
    display(angulo);
  
%--------Grafica_valores----------------
[x0, y0] = pol2cart(angulo, 40);
if radio<40
   m=plot([x,x0],[y,y0],'r','LineWidth',3);
else
   m=plot([0,x0],[0,y0],'Color',[0 1 1],'LineWidth',3);
end
drawnow;
delete(m);
end
fclose(s)
delete(s)
clear s