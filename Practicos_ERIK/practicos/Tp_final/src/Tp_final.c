/*PCLK=100MHz
 BaudRate=9600
 Solo envia
 matlab recibe y grafica
 el envio debe ser en dos partes para el radio y el angulo
*/
#define AddrSCS 0x400FC1A0
#define AddrCCLKCFG 0x400FC104
#define AddrPCLKSEL0 0x400FC1A8
#define AddrPCLKSEL1 0x400FC1AC
#define AddrCLKSRCSEL 0x400FC10C
#define AddrPLL0CFG 0x400FC084
#define AddrPLL0FEED 0x400FC08C
#define AddrPLL0CON 0x400FC080
#define AddrPLL0STAT 0x400FC088

#define AddrPCONP 0x400FC0C4
#define AddrU2LCR 0x4009800C
#define AddrU2DLL 0x40098000
#define AddrU2DLM 0x40098004
#define AddrU2IER 0x40098004
#define AddrISER0 0xE000E100
#define AddrU2THR 0x40098000
#define AddrU2LSR 0x40098014
#define AddrU2RBR 0x40098000

//-------GPIO0--------------
#define AddrFIO0DIR 0X2009C000
#define AddrFIO0SET 0X2009C018
#define AddrFIO0CLR 0X2009C01C
#define AddrFIO0PIN 0X2009C014

//---------GPIO2---------------
#define AddrFIO2DIR 0X2009C040
#define AddrFIO2SET 0X2009C058
#define AddrFIO2CLR 0x2009C05C
#define AddrFIO2PIN 0x2009C054

#define AddrPINSEL3 0X4002C00C
//----------GPIO_INTERRUPT-----------
#define AddrPINSEL0 0X4002C000
#define AddrIO0IntEnR 0X40028090 //registros asociados a las
#define AddrIO0IntEnF 0X40028094 // interrupciones por GPIO
#define AddrIO0IntStatR 0X40028084
#define AddrIO0IntStatF 0X40028088
#define AddrIO0IntClr 0X4002808C

//--------TIMER0------------
#define AddrT0EMR 0X4000403C
#define AddrT0MCR 0X40004014
#define AddrT0MR0 0X40004018
#define AddrT0TCR 0X40004004
#define AddrT0IR  0X40004000
#define AddrT0PR  0X4000400C

//--------TIMER1------------
#define AddrT1EMR 0X4000803C
#define AddrT1MCR 0X40008014
#define AddrT1MR0 0X40008018
#define AddrT1TCR 0X40008004
#define AddrT1IR 0X40008000
#define AddrT1PR 0X4000800C
#define AddrT1CCR 0X40008028
#define AddrT1CR0 0x4000802C

//----------TIMER2-------------
#define AddrT2EMR 0X4009003C
#define AddrT2MCR 0X40090014
#define AddrT2MR0 0X40090018
#define AddrT2TCR 0X40090004
#define AddrT2IR 0X40090000
#define AddrT2PR 0X4009000C

//------TIMER3-----------
#define AddrT3EMR 0X4009403C
#define AddrT3MCR 0x40094014
#define AddrT3MR0 0X40094018
#define AddrT3TCR 0X40094004
#define AddrT3IR 0X40094000
#define AddrT3PR 0X4009400C

//------------ADC-----------------
#define AddrAD0CR 0x40034000
#define AddrAD0INTEN 0x4003400C
#define AddrPINMODE1 0x4002C044
#define AddrPINSEL1 0x4002C004
#define AddrAD0DR0 0x40034010


unsigned int volatile *const SCS=(unsigned int*)AddrSCS;
unsigned int volatile *const CCLKCFG=(unsigned int*)AddrCCLKCFG;
unsigned int volatile *const PCLKSEL0=(unsigned int*)AddrPCLKSEL0;
unsigned int volatile *const PCLKSEL1=(unsigned int*)AddrPCLKSEL1;
unsigned int volatile *const CLKSRCSEL=(unsigned int*)AddrCLKSRCSEL;
unsigned int volatile *const PLL0CFG=(unsigned int*)AddrPLL0CFG;
unsigned int volatile *const PLL0FEED=(unsigned int*)AddrPLL0FEED;
unsigned int volatile *const PLL0CON=(unsigned int*)AddrPLL0CON;
unsigned int volatile *const PLL0STAT=(unsigned int*)AddrPLL0STAT;

unsigned int volatile *const PCONP=(unsigned int*)AddrPCONP;
unsigned int volatile *const PINSEL0=(unsigned int*)AddrPINSEL0;
unsigned int volatile *const U2LCR=(unsigned int*)AddrU2LCR;
unsigned int volatile *const U2DLL=(unsigned int*)AddrU2DLL;
unsigned int volatile *const U2DLM=(unsigned int*)AddrU2DLM;
unsigned int volatile *const U2IER=(unsigned int*)AddrU2IER;
unsigned int volatile *const ISER0=(unsigned int*)AddrISER0;
unsigned int volatile *const U2THR=(unsigned int*)AddrU2THR;
unsigned int volatile *const U2LSR=(unsigned int*)AddrU2LSR;
unsigned int volatile *const U2RBR=(unsigned int*)AddrU2RBR;

unsigned int volatile *const FIO0DIR=(unsigned int*)AddrFIO0DIR;
unsigned int volatile *const FIO0SET=(unsigned int*)AddrFIO0SET;
unsigned int volatile *const FIO0CLR=(unsigned int*)AddrFIO0CLR;
unsigned int volatile *const FIO0PIN=(unsigned int*)AddrFIO0PIN;

unsigned int volatile *const FIO2DIR= (unsigned int*) AddrFIO2DIR;
unsigned int volatile *const FIO2SET= (unsigned int*) AddrFIO2SET;
unsigned int volatile *const FIO2CLR= (unsigned int*) AddrFIO2CLR;
unsigned int volatile *const FIO2PIN= (unsigned int*) AddrFIO2PIN;

unsigned int volatile *const PINSEL3=(unsigned int*)AddrPINSEL3;

unsigned int volatile *const IO0IntEnR=(unsigned int*) AddrIO0IntEnR;
unsigned int volatile *const IO0IntEnF= (unsigned int*) AddrIO0IntEnF;
unsigned int volatile *const IO0IntStatR= (unsigned int*) AddrIO0IntStatR;
unsigned int volatile *const IO0IntStatF= (unsigned int*) AddrIO0IntStatF;
unsigned int volatile *const IO0IntClr= (unsigned int*) AddrIO0IntClr;

unsigned int volatile *const T0EMR=(unsigned int*)AddrT0EMR;
unsigned int volatile *const T0MCR=(unsigned int*)AddrT0MCR;
unsigned int volatile *const T0MR0=(unsigned int*)AddrT0MR0;
unsigned int volatile *const T0TCR=(unsigned int*)AddrT0TCR;
unsigned int volatile *const T0IR=(unsigned int*)AddrT0IR;
unsigned int volatile *const T0PR=(unsigned int*)AddrT0PR;

unsigned int volatile *const T1EMR=(unsigned int*)AddrT1EMR;
unsigned int volatile *const T1MCR=(unsigned int*)AddrT1MCR;
unsigned int volatile *const T1MR0=(unsigned int*)AddrT1MR0;
unsigned int volatile *const T1TCR=(unsigned int*)AddrT1TCR;
unsigned int volatile *const T1IR=(unsigned int*)AddrT1IR;
unsigned int volatile *const T1PR=(unsigned int*)AddrT1PR;
unsigned int volatile *const T1CCR=(unsigned int*)AddrT1CCR;
unsigned int volatile *const T1CR0=(unsigned int*)AddrT1CR0;

unsigned int volatile *const T2EMR=(unsigned int*)AddrT2EMR;
unsigned int volatile *const T2MCR=(unsigned int*)AddrT2MCR;
unsigned int volatile *const T2MR0=(unsigned int*)AddrT2MR0;
unsigned int volatile *const T2TCR=(unsigned int*)AddrT2TCR;
unsigned int volatile *const T2IR=(unsigned int*)AddrT2IR;
unsigned int volatile *const T2PR=(unsigned int*)AddrT2PR;

unsigned int volatile *const T3EMR=(unsigned int*)AddrT3EMR;
unsigned int volatile *const T3MCR=(unsigned int*)AddrT3MCR;
unsigned int volatile *const T3MR0=(unsigned int*)AddrT3MR0;
unsigned int volatile *const T3TCR=(unsigned int*)AddrT3TCR;
unsigned int volatile *const T3IR=(unsigned int*)AddrT3IR;
unsigned int volatile *const T3PR=(unsigned int*)AddrT3PR;

unsigned int volatile*const AD0CR = (unsigned int*) AddrAD0CR;
unsigned int volatile*const AD0INTEN = (unsigned int*) AddrAD0INTEN;
unsigned int volatile*const PINMODE1 = (unsigned int*) AddrPINMODE1;
unsigned int volatile*const PINSEL1 = (unsigned int*) AddrPINSEL1;
unsigned int volatile*const AD0DR0 = (unsigned int*) AddrAD0DR0;

//-------funciones----------
void clockConfig(void);
void uartConfig(void);
void Timer0Config(void);
void Timer1Config(void);
void TIMER2Config(void);
void Timer3Config(void);
void GPIOIntConfig(void);
void adcConfig();
extern void Pulso(void);
void muestra(int);
void dist(void);
void enviar(void);
void paso(void);
void direc(int);
void stop(void);
void start(void);
int antirebote(int);
int retardo(unsigned int);
void ang(void);

//------Variables----------
int direccion=0;
int Puls,PulsAnt;
int pasos=0;
int distancia=0;
int bandera=0;				//b0=distancia b1=angulo b2=enviar_datos b3=conversor/giro b4=ADC_complete
							//b5=pasos determinados b6=motor en 0 b7=angulo determinado b8=dist/ang
int radio,radio1,radio2;
int dist1,dist2,dist3,dist4;
int angulo,angulo1,angulo2;
int ang1,ang2,ang3,ang4;
int valor1,valor2,valor3,valor4;
int pasos,pasos2,contador;
int paso1,paso2;



int main(void){
	clockConfig();
	uartConfig();
	TIMER2Config();
	GPIOIntConfig();
	adcConfig();
	Timer0Config();
	Timer1Config();
	Timer3Config();

	valor1=0;
	valor2=0;
	valor3=0;
	valor4=0;

	*FIO0DIR|=(1<<2);    		//P0.2 salida, trigger sensor
	*FIO2DIR |=255;             //P2.0 a P2.7 salidas segmentos del display
	*FIO2DIR|=(29<<8);			//P2.8 a P2.12(sin P2.9 display enable)

	*FIO0DIR|=(15<<6);	//P0.8 y P0.9 Salidas
	*FIO0CLR|=(15<<6);  //salidas en 0

	*T0TCR|=1;					//enciende y reinicia el TMR0
	*T0TCR|=(1<<1);
	*T0TCR&=~(1<<1);
	*T3TCR|=1;
	*T1TCR|=1;					//enciende el TMR1
	*T1TCR|=(1<<1);
				//habilito el registro del control del timer
	*ISER0|=(1<<1);				//habilita interrupciones
	*ISER0|=(1<<4);
	*ISER0|=(1<<2);

	stop();			//para el motor
	start();
	while(1){
		if((bandera&1)==1)
			dist();
		if(((bandera>>1)&1)==1)
			ang();
		if(((bandera>>2)&1)==1)
			enviar();
	}
	return 0;
}

void uartConfig(void){
	*PCONP|=(1<<24); 		//UART 2 power/clock control bit
	*PCLKSEL1&=~(3<<16);	//Peripheral clock selection for UART2:CCLK/4
	*U2LCR|=3;				//Word length select:8-BIT character length, stop bit select:1stop bit,
	//Parity Enable:Disable parity generation and checking, Break Control: Disable break transmission
	*U2LCR|=(1<<7);			//enable access to divisor latches
	*U2DLL=163;				//The UARTn Divisor Latch LSB Register, along with the UnDLM
	*U2DLM=0;				//register, determines the baud rate of the UARTn
	*U2LCR&=~(1<<7); 		//Disable access to Divisor Latches
	*PINSEL0|=(5<<20);		//Configure P0.10 as Tx and P0.11 as Rx
	//*U2IER=1;				//Enables the REcieve Data Available interrupt for UARTn
	*ISER0|=(1<<7);			//UART2 Interrupt Enable
}


void enviar(void){
	while((*U2LSR&(1<<5))==0);//check if UnTHR contains valid data or is empty
	*U2THR=radio1;
	while((*U2LSR&(1<<5))==0);//check if UnTHR contains valid data or is empty
	*U2THR=radio2;
	while((*U2LSR&(1<<5))==0);//check if UnTHR contains valid data or is empty
	*U2THR=angulo1;
	while((*U2LSR&(1<<5))==0);//check if UnTHR contains valid data or is empty
	*U2THR=angulo2;
	bandera&=~(1<<2);
}

void clockConfig(void){
	*SCS=32;				//Main Oscillator is enabled
	while((*SCS&(1<<6))==0); //Wait for Oscillator to be ready
	//-------------------------------------------------------------------
	*CCLKCFG=0x3;			//Setup Clock Divider:pllclk is divided by 4 to produce the CPU clock
	//-------------------------------------------------------------------
	*PCLKSEL0=0x0;			//Peripheral Clock Selection
	*PCLKSEL1=0x0;
	//-------------------------------------------------------------------
	*CLKSRCSEL=0x1;			//Select Clock Source for PLL0
	//-------------------------------------------------------------------
	*PLL0CFG=0x50063;		//configure PLL0: M=100 N=6
	*PLL0FEED=0xAA;
	*PLL0FEED=0x55;
	//-------------------------------------------------------------------
	*PLL0CON=0x01;			//PLL0 Enable
	*PLL0FEED=0xAA;
	*PLL0FEED=0x55;
	while(!(*PLL0STAT&(1<<26))); //Wait for PLOCK0
	//-------------------------------------------------------------------
	*PLL0CON=0x03;			//PLL0 Enable & Connect
	*PLL0FEED=0xAA;
	*PLL0FEED=0x55;
	while(!(*PLL0STAT&((1<<25)|(1<<24)))); //Wait for PLLC0_STAT & PLLE0_STAT
}

void Timer0Config(void){
	*PCONP|=(3<<1);				//enciende TMR0
	*PCLKSEL0&=~(3<<2);			//TMR0clock=CCLK/4
	*T0MCR|=3;					//interrumpe y reinicia el timer
	*T0MR0=100000;				//envia el pulso cada medio segundo
	*T0PR=124;
}

void Timer1Config(void){
	*PCLKSEL0|=(1<<4);			//enciende TMR1
	*PINSEL3|=(3<<4);			//habilita CAP1.0
	*T1CCR|=7;					//CAP1.0 interrumpe por ambos flancos
}

void TIMER2Config(void){
	*PCONP|=(1<<22); //TIMER2 ON
	*PCLKSEL1|=(1<<12);	//TIMER2 CLOCK=CCLK
	*T2MCR|=3;			//interrupcion timer habilitada

	*T2MR0=100000;
	*T2PR=49;
	*ISER0|=(1<<3);
	*T2TCR|=1;


}

void Timer3Config(void){
	*PCONP|=(1<<23);		//enciendo el periferico del timer
	*PCLKSEL1&=~(3<<14);	//peripheral clock: system clock
	*T3MCR|=3;			//reset on MR0 the TC will be reset if MR0 matches it. interrup on MR0: an interrup is
							//generated when MR0 matches the value in the TC
	*T3MR0=25000;		//match register 0 6450
	*T3PR=4;
}

void GPIOIntConfig(void){
		*PINSEL0 &=~(3<<30);  //configura interrupciones GPIO
		*FIO0DIR &=~(15<<15);
		*IO0IntEnF|=(1<<15); //enable falling edge interrupt for P.0.15
		*IO0IntEnF|=(1<<16); //enable falling edge interrupt for P.0.16
		*IO0IntEnF|=(1<<17); //enable falling edge interrupt for P.0.17
		*IO0IntEnF|=(1<<18); //enable falling edge interrupt for P.0.18

		*ISER0|=(1<<21);
}

void adcConfig()
{
	*PCONP|=(1<<12);	// habilito periferico
	*AD0CR|=1<<21;		// habilito A/D
	*PCLKSEL0|=(3<<24);	// selecciono PCLK_ADC
	*AD0CR&=~(255<<8);	// clock colocado a 13MHZ
	*AD0CR&=~(1<<16);		// convierte constantemente
	*PINMODE1|=1<<15;	// saco resistencias
	*PINSEL1|=(1<<14);	// selecciono el pin como AD0.0
	*AD0INTEN=1;
	*ISER0|=(1<<22);	// habilito interrupcion externa
}

void TIMER0_IRQHandler(void){
	Pulso();					//envia el pulso
	*T0IR=1;
	*T1TCR|=(1<<1);
}

void TIMER1_IRQHandler(void){
	static int a=0;
	if(a==1){
		distancia=*T1CR0;//-1750;		//calcula la distancia del objeto
		bandera|=1;
		a=0;
	}
	else{
		*T1TCR&=~(1<<1);	//reinicia el TMR1
		a=1;
	}
	*T1IR|=(1<<4);
}

void TIMER2_IRQHandler(void){
	if(((bandera>>3)&1)==1){
		*AD0CR|=(1<<24);
		if (((bandera>>4)&1)==1){
		paso();
		bandera&=~(1<<4);
		}
	}
	else if(((bandera>>5)&1)==0)
		paso();
	if((bandera>>5)&1){
		paso();
		contador--;
		if(contador==0){
			contador=pasos2;
			if(direccion==0)
			direc(1);
			else
				direc(0);
		}
	}
	bandera|=(1<<1);
	bandera|=(1<<2);
	*T2IR=1;
}

void TIMER3_IRQHandler(void){		//P2.0 a P2.7 Segmentos-- P2.8(sin P2.9) a P2.12 multiplexado
	static int display=1;
	if(((bandera>>8)&1)==0){
		valor1=dist1;
		valor2=dist2;
		valor3=dist3;
		valor4=dist4;
	}
	else{
		valor1=ang1;
		valor2=ang2;
		valor3=ang3;
		valor4=ang4;
	}
	display=(display*2);
	if(display==16){
		display=1;
	}
	if (display==1){
		*FIO2CLR|=(1<<12);
		*FIO2SET|=(1<<8);
		muestra(valor1);
	}
	if(display==2){
		*FIO2CLR|=(1<<8);
		*FIO2SET|=(1<<10);
		muestra(valor2);
	}
	if(display==4){
		*FIO2CLR|=(1<<10);
		*FIO2SET|=(1<<11);
		muestra(valor3);
	}
	if(display==8){
		*FIO2CLR|=(1<<11);
		*FIO2SET|=(1<<12);
		muestra(valor4);
	}
	if(valor4==12){
			*FIO2CLR|=(1<<8);
		}
	*T3IR|=1;				//MR0 interrupt flag for match channel 0
}

void ADC_IRQHandler(void)						//*se deberan contar los pasos para girar el motor entre esos limites
{
	static int lim=43;                      	//limite superior inicial 36 (el potenciometro debera estar en el 0)
	unsigned short volatile ADC0Value=0;
	if((bandera>>6)&1){
		lim=43;
		bandera&=~(1<<6);
	}

	ADC0Value=(*AD0DR0>>4)&0xFFF;
	if (ADC0Value<(lim-85)){				//el valor es menor al anterior entonces se mueve un paso en la direccion 1
		lim-=73;						//se actualiza el limite
		direc(1);
		bandera|=(1<<4);
	}
	if (ADC0Value>(lim+85)){	//el valor es mayor al anterior entonces se mueve un paso a la derecha
		lim+=73;					// actualiza el limite
		direc(0);
		bandera|=(1<<4);
	}
}

void paso(void){
	static int cont=0;
		if (direccion==0){			// dependiendo de la direccion genera la secuencia en un sentido o en el otro
			pasos++;
			cont++;
			if(cont==4)
				cont=0;
		}
		if (direccion==1){
			pasos--;
			cont--;
			if(cont<0)
				cont=3;
		}
	switch (cont){					//P0.6=In1 P0.7=In2 P0.8=In3  P0.9=In4
		case 0:
			*FIO0CLR|=(15<<6);//(5<<7);		//	  | paso1 | paso2 | paso3 | paso4 |
			*FIO0SET|=(5<<6);		//In1 |	  1	  |	  1	  |	  0	  |	  0	  |
			break;					//In2 |	  0	  |	  0	  |	  1	  |	  1	  |
		case 1:						//In3 |	  1	  |	  0	  |   0	  |   1   |
			*FIO0CLR|=(15<<6);//(3<<7);		//In4 |	  0	  |	  1	  |   1	  |	  0   |
			*FIO0SET|=(9<<6);
			break;
		case 2:
			*FIO0CLR|=(15<<6);//(5<<6);
			*FIO0SET|=(5<<7);
			break;
		case 3:
			*FIO0CLR|=(15<<6);//(9<<6);
			*FIO0SET|=(3<<7);
			break;
	}

}

void EINT3_IRQHandler(void){
	static int a=0;
	static int b=0;
	static int c=0;
	if((*IO0IntStatF>>15)&1){		//P0.15 Angulo 0
		retardo(2000);
		Puls=(*FIO0PIN>>15)&1;
		PulsAnt=Puls;
		Puls=antirebote(Puls);
		retardo(4000);
		if(Puls==0&& PulsAnt==0){
			stop();
			if(((bandera>>6)&1)==0){
			if (a==0){
				direc(0);
				a=1;
				pasos=0;
			}
			else{
				direc(1);
				a=0;
			}
			start();}
			else{
				bandera&=~(1<<5);
				bandera|=(1<<3);
				pasos=0;
				//bandera&=~(1<<6);
				start();
			}
		}
	}
	if((*IO0IntStatF>>16)&1){			//habilita el cambio de posicion mediante el uso del potenciometro
		retardo(30000);
		Puls=(*FIO0PIN>>16)&1;
			PulsAnt=Puls;
			Puls=antirebote(Puls);
			retardo(50000);
			if(Puls==0&& PulsAnt==0){//status of rising edge interrupt for P.0.15
				valor1=ang1;
				valor2=ang2;
				valor3=ang3;
				valor4=ang4;
				stop();
				if(b==0){
					*T2PR=19;
					if(angulo>0){
						direc(1);
						start();
						bandera|=(1<<6);
					}
					else{
					bandera&=~(1<<5);
					bandera|=(1<<3);
					start();
					}
					b=1;}
				else if(b==1){
					paso1=pasos;
					b=2;
					start();
				}
				else if(b==2){
					paso2=pasos;
					bandera&=~(1<<3);
					if(paso2>paso1){
						pasos2=paso2-paso1;
						direc(1);
					}
					else{
						pasos2=paso1-paso2;
						direc(0);
					}
					contador=pasos2;
					bandera|=(1<<5);
					bandera|=(1<<7);
					b=3;
					if (paso2==paso1){
						stop();
					}
					else{
						*T2PR=49;
						start();
					}

				}
			}
	}
	if((*IO0IntStatF>>17)&1){			//gira el motor una cantidad determinada de pasos
			retardo(30000);
			Puls=(*FIO0PIN>>17)&1;
				PulsAnt=Puls;
				Puls=antirebote(Puls);
				retardo(50000);
				if(Puls==0&& PulsAnt==0){//status of rising edge interrupt for P.0.15
					if(c==0){
						bandera|=(1<<8);
						c=1;
					}
					else{
						bandera&=~(1<<8);
						c=0;
					}
				}
		}
	if((*IO0IntStatF>>18)&1){			//gira el motor una cantidad determinada de pasos
				retardo(1000);
				Puls=(*FIO0PIN>>18)&1;
					PulsAnt=Puls;
					Puls=antirebote(Puls);
					retardo(5000);
					if(Puls==0&& PulsAnt==0){//status of rising edge interrupt for P.0.15
						bandera&=~(1<<7);
						bandera&=~(1<<3);
						bandera&=~(1<<5);
						*T2PR=49;
						start();
						b=0;
					}
			}
	*IO0IntClr|=(1<<15);
	*IO0IntClr|=(1<<16);
	*IO0IntClr|=(1<<17);
	*IO0IntClr|=(1<<18);
}

void ang(void){

	if (((bandera>>7)&1)==0)
	angulo=75*pasos;
	else{
		if(direccion==0){
			if(paso1>paso2){
				angulo=(paso1-contador)*75;
			}
			else
				angulo=(paso2-contador)*75;
		}

		else{
			if(paso1>paso2)
			angulo=(paso2+contador)*75;
			else
				angulo=(paso1+contador)*75;
		}

	}

	angulo=angulo/10;
	angulo1=(angulo&255);
	angulo2=(angulo>>8)&255;
	ang2=0;
	ang3=0;
	ang4=0;
	ang1=13;
		while(angulo>=100){
			angulo=angulo-100;
			ang4++;
			}
		while(angulo>=10){
			angulo=angulo-10;
			ang3++;
		}
		ang2=angulo;
		bandera&=~(1<<1);
}

void dist(void){
	distancia=distancia/5800; //ver el termino a dividir
	radio1=distancia&255;
	radio2=(distancia>>8)&255;
	dist1=0;			//tiene max de 60 cm
	dist2=0;
	dist3=0;
	dist4=0;
	if (distancia>=100){
		//dist1=12;
		while(distancia>100){
			distancia=distancia-100;
			dist4++;
		}
		/*while(distancia>10){
						distancia=distancia-10;
						dist3++;
					}
					dist2=distancia;*/
				}
	else{
		dist1=11;
		dist2=10;
		while(distancia>=10){
			distancia=distancia-10;
			dist4++;
		}
		dist3=distancia;
	}
	bandera&=~1;
}

void muestra(int numero){				//valor del display en logica negativa
			switch (numero){			//modificar los valores que se envian
					case 0 :
						*FIO2SET|=255;
						*FIO2CLR|=63;
						break;
					case 1 :
						*FIO2SET|=255;
						*FIO2CLR|=6;
						break;
					case 2 :
						*FIO2SET|=255;
						*FIO2CLR|=91;
						break;
					case 3 :
						*FIO2SET|=255;
						*FIO2CLR|=79;
						break;
					case 4 :
						*FIO2SET|=255;
						*FIO2CLR|=102;
						break;
					case 5 :
						*FIO2SET|=255;
						*FIO2CLR|=109;
						break;
					case 6 :
						*FIO2SET|=255;
						*FIO2CLR|=125;
						break;
					case 7 :
						*FIO2SET|=255;
						*FIO2CLR|=7;
						break;
					case 8 :
						*FIO2CLR|=127;
						break;
					case 9 :
						*FIO2SET|=255;
						*FIO2CLR|=103;
						break;
					case 10 :
						*FIO2SET|=127;
						*FIO2CLR|=57;
						break;
					case 11 :
						*FIO2SET|=127;
						*FIO2CLR|=84;
						break;
					case 12 :
						*FIO2SET|=127;
						*FIO2CLR|=55;
						break;
					case 13 :
						*FIO2SET|=127;
						*FIO2CLR|=99;
}}

void start(void){			//enciende el motor
	*T2MCR|=1;
	*T2TCR&=~(1<<1);
}

void stop(void){			//apaga el motor
	*FIO0CLR|=(15<<6);
	*T2MCR&=~1;
	*T2TCR|=(1<<1);
}

void direc(int a){			//cambia la direccion del motor
	if (a==1)
		direccion=1;
	else
		direccion=0;
}

int antirebote(int SampleA)			//antirebote para flanco de bajada
{
static int SampleB = 0;
static int SampleC = 0;
static int UltimoResultado = 0;

UltimoResultado = (UltimoResultado & (SampleA | SampleB | SampleC)) | (SampleA & SampleB & SampleC);
SampleC = SampleB;
SampleB = SampleA;
return UltimoResultado;
}

int retardo(unsigned int time){
unsigned int i;
for(i=0;i<time;i++); //lazo de demora
return 0;
}
