.syntax unified
.global Pulso
.type Pulso,function

Pulso: 	LDR r1,FIO0SET;
		MOV r0,#0x0004;  // P0.2 Trigger
		STR r0,[r1];
		MOV r2,#0x64;    //78
LOOP:	MOV r3,#0x21;
LOOP2:	SUBS r3,#0x1;
		BNE LOOP2;
		SUBS r2,#0x1;
		BNE LOOP;
		NOP;
		LDR r1,FIO0CLR;
		MOV r0,#0x0004;
		STR r0,[r1];
		MOV PC,LR;

FIO0SET:	.word 	0x2009C018
FIO0CLR:	.word	0x2009C01C

.END
