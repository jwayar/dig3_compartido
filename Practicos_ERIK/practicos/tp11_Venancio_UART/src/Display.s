.syntax unified
.global Display
.type Display, function

Display: 		LDR R1,=letraS		//P2.0 a P2.6 segmentos del display
				LSL R0,#2
				ADD R0,R1  //R0=R1+R0*4.  Esto es para saltar los espacios de memoria que
									//conforman la tabla.
				LDR R0,[R0]         //Guarda en R0 el valor apuntado por R0 en la tabla.
				LDR R1,FIO2CLR
				MOV R2, #0x0000007F
				STR	R2,[R1]         // Apago leds del display

				LDR R1,FIO2SET
				STR R0,[R1]         //Muestro letra en el display vía FIO2SET.
				MOV PC,LR


FIO2CLR:	.word	0x2009C05C
FIO2SET:	.word	0x2009C058

letraS:		.word   0x6D  //Letra S(dato correcto)
letraE:		.word   0x79  //Letra E(error de paridad)
.end
