#define AddrPCONP 0X400FC0C4
#define AddrPCLKSEL0 0X400FC1A8
#define AddrPINSEL3 0X4002C00C
#define AddrISER0 0XE000E100

//--------TIMER0------------
#define AddrT0EMR 0X4000403C
#define AddrT0MCR 0X40004014
#define AddrT0MR0 0X40004018
#define AddrT0TCR 0X40004004
#define AddrT0IR  0X40004000
#define AddrT0PR  0X4000400C

//--------TIMER1------------
#define AddrT1EMR 0X4000803C
#define AddrT1MCR 0X40008014
#define AddrT1MR0 0X40008018
#define AddrT1TCR 0X40008004
#define AddrT1IR 0X40008000
#define AddrT1PR 0X4000800C
#define AddrT1CCR 0X40008028
#define AddrT1CR0 0x4000802C

//--------GPIO-------------
#define AddrFIO0DIR 0X2009C000
#define AddrFIO0SET 0X2009C018
#define AddrFIO0CLR 0X2009C01C
#define AddrFIO0PIN 0X2009C014

unsigned int volatile *const PCONP=(unsigned int*)AddrPCONP;
unsigned int volatile *const PCLKSEL0=(unsigned int*)AddrPCLKSEL0;
unsigned int volatile *const PINSEL3=(unsigned int*)AddrPINSEL3;
unsigned int volatile *const ISER0=(unsigned int*)AddrISER0;

unsigned int volatile *const T0EMR=(unsigned int*)AddrT0EMR;
unsigned int volatile *const T0MCR=(unsigned int*)AddrT0MCR;
unsigned int volatile *const T0MR0=(unsigned int*)AddrT0MR0;
unsigned int volatile *const T0TCR=(unsigned int*)AddrT0TCR;
unsigned int volatile *const T0IR=(unsigned int*)AddrT0IR;
unsigned int volatile *const T0PR=(unsigned int*)AddrT0PR;

unsigned int volatile *const T1EMR=(unsigned int*)AddrT1EMR;
unsigned int volatile *const T1MCR=(unsigned int*)AddrT1MCR;
unsigned int volatile *const T1MR0=(unsigned int*)AddrT1MR0;
unsigned int volatile *const T1TCR=(unsigned int*)AddrT1TCR;
unsigned int volatile *const T1IR=(unsigned int*)AddrT1IR;
unsigned int volatile *const T1PR=(unsigned int*)AddrT1PR;
unsigned int volatile *const T1CCR=(unsigned int*)AddrT1CCR;
unsigned int volatile *const T1CR0=(unsigned int*)AddrT1CR0;

unsigned int volatile *const FIO0DIR=(unsigned int*)AddrFIO0DIR;
unsigned int volatile *const FIO0SET=(unsigned int*)AddrFIO0SET;
unsigned int volatile *const FIO0CLR=(unsigned int*)AddrFIO0CLR;
unsigned int volatile *const FIO0PIN=(unsigned int*)AddrFIO0PIN;

extern void Pulso(void);
int distancia=0;

int main(void){
	*FIO0DIR|=(1<<2);    		//P0.2 salida, trigger sensor

	*PCONP|=(3<<1);				//enciende TMR0
	*PCLKSEL0|=(1<<2);			//TMR0clock=CCLK
	*T0MCR|=3;					//interrumpe y reinicia el timer
	*T0MR0=4000;

	*PCLKSEL0|=(1<<4);			//enciende TMR1
	*PINSEL3|=(3<<4);			//habilita CAP1.0
	*T1CCR|=7;					//CAP1.0 interrumpe por ambos flancos

	*ISER0|=(1<<1);				//habilita interrupciones
	*ISER0|=(1<<2);

	*T0TCR|=1;					//enciende y reinicia el TMR0
	*T1TCR|=1;					//enciende el TMR1
	*T1TCR|=(1<<1);

	while(1){

	}
	return 0;
}

void TIMER0_IRQHandler(void){
	Pulso();					//envia el pulso
	*T0IR=1;
}

void TIMER1_IRQHandler(void){
	static int bandera=0;
	if(bandera==1){
		distancia=*T1CR0-1750;		//calcula la distancia del objeto
		distancia=distancia/240; //ver el termino a dividir
		bandera=0;
		*T1TCR|=(1<<1);
	}
	else{
		*T1TCR&=~(1<<1);	//reinicia el TMR1
		bandera=1;
	}
	*T1IR=1;
}
