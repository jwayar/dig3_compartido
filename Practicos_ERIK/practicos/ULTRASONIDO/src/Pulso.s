.syntax unified
.global Pulso
.type Pulso,function

Pulso: 	LDR r1,FIO0SET;
		MOV r0,#0x0004;  // P0.2 Trigger
		STR r0,[r1];
		MOV r2,#0x28;    //78
LOOP:	SUBS r2,#0x1;
		BNE LOOP;
		LDR r1,FIO0CLR;
		MOV r0,#0x0004;
		STR r0,[r1];
		MOV PC,LR;

FIO0SET:	.word 	0x2009C018
FIO0CLR:	.word	0x2009C01C

.END
