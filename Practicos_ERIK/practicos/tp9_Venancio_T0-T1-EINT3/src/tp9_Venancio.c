#define AddrFIO2DIR 0X2009C040
#define AddrFIO2SET 0X2009C058
#define AddrFIO2CLR 0x2009C05C
#define AddrFIO2PIN 0x2009C054

#define AddrPINSEL0 0X4002C000

#define AddrPCONP 0X400FC0C4
#define AddrPCLKSEL0 0X400FC1A8
#define AddrPINSEL3 0X4002C00C
//-------TIMER0----------
#define AddrT0EMR 0X4000403C
#define AddrT0MCR 0X40004014
#define AddrT0MR0 0X40004018
#define AddrT0TCR 0X40004004
#define AddrT0IR 0X40004000
#define AddrT0PR 0X4000400C
#define AddrT0CCR 0X40004028
#define AddrT0CR0 0X4000402C
//------TIMER1-----------
#define AddrT1EMR 0X4000803C
#define AddrT1MCR 0X40008014
#define AddrT1MR0 0X40008018
#define AddrISER0 0XE000E100
#define AddrT1TCR 0X40008004
#define AddrT1IR 0X40008000
#define AddrT1PR 0X4000800C

unsigned int volatile *const FIO2DIR= (unsigned int*) AddrFIO2DIR;
unsigned int volatile *const FIO2SET= (unsigned int*) AddrFIO2SET;
unsigned int volatile *const FIO2CLR= (unsigned int*) AddrFIO2CLR;
unsigned int volatile *const FIO2PIN= (unsigned int*) AddrFIO2PIN;

unsigned int volatile *const PINSEL0= (unsigned int*) AddrPINSEL0;

unsigned int volatile *const PCONP=(unsigned int*)AddrPCONP;
unsigned int volatile *const PCLKSEL0=(unsigned int*)AddrPCLKSEL0;
unsigned int volatile *const PINSEL3=(unsigned int*)AddrPINSEL3;

unsigned int volatile *const T0EMR=(unsigned int*)AddrT0EMR;
unsigned int volatile *const T0MCR=(unsigned int*)AddrT0MCR;
unsigned int volatile *const T0MR0=(unsigned int*)AddrT0MR0;
unsigned int volatile *const T0TCR=(unsigned int*)AddrT0TCR;
unsigned int volatile *const T0IR=(unsigned int*)AddrT0IR;
unsigned int volatile *const T0PR=(unsigned int*)AddrT0PR;
unsigned int volatile *const T0CCR=(unsigned int*)AddrT0CCR;
unsigned int volatile *const T0CR0=(unsigned int*)AddrT0CR0;

unsigned int volatile *const T1EMR=(unsigned int*)AddrT1EMR;
unsigned int volatile *const T1MCR=(unsigned int*)AddrT1MCR;
unsigned int volatile *const T1MR0=(unsigned int*)AddrT1MR0;
unsigned int volatile *const ISER0=(unsigned int*)AddrISER0;
unsigned int volatile *const T1TCR=(unsigned int*)AddrT1TCR;
unsigned int volatile *const T1IR=(unsigned int*)AddrT1IR;
unsigned int volatile *const T1PR=(unsigned int*)AddrT1PR;

int cuenta;
int decenas,unidades;
int main(void){
	cuenta=0;
	decenas=0;
	unidades=0;
	*FIO2DIR |=127;
	*PINSEL0 &=~(3<<30);

	*PCONP|=(3<<1);		//enciendo el periferico del timer
	*PCLKSEL0|=(3<<4);	//peripheral clock: system clock
	*PINSEL3|=(3<<12);	//pins: match1.0
	*PINSEL3|=(3<<18);	//pins: match1.1
	*T1EMR|=(15<<4);
	*T1EMR|=1;
	*T1EMR&=~(1<<1);
	*T1MCR|=3;			//reset on MR0 the TC will be reset if MR0 matches it. interrup on MR0: an interrup is
						//generated when MR0 matches the value in the TC
	*T1MR0=6450;		//match register 0
	*ISER0|=(1<<2);
	*T1TCR|=1;			//habilito el registro del control del timer

	*PCLKSEL0&=~(3<<2);
	*PINSEL3|=(3<<20);
	*T0CCR|=(3<<1);
	*T0TCR|=1;

	*ISER0|=(3<<1);
	*ISER0|=(1<<21); //external interrupt 3 interrupt enable

while(1){

}
return 0;
}

void TIMER1_IRQHandler(void){
	if((*T1EMR&1)==1){
		switch (decenas){
				case 0 :
					*FIO2CLR|=127;
					*FIO2SET|=63;
					break;
				case 1 :
					*FIO2CLR|=127;
					*FIO2SET|=6;
					break;
				case 2 :
					*FIO2CLR|=127;
					*FIO2SET|=91;
					break;
				case 3 :
					*FIO2CLR|=127;
					*FIO2SET|=79;
					break;
				case 4 :
					*FIO2CLR|=127;
					*FIO2SET|=102;
					break;
				case 5 :
					*FIO2CLR|=127;
					*FIO2SET|=109;
					break;
				case 6 :
					*FIO2CLR|=127;
					*FIO2SET|=125;
					break;
				case 7 :
					*FIO2CLR|=127;
					*FIO2SET|=7;
					break;
				case 8 :
					*FIO2SET|=127;
					break;
				case 9 :
					*FIO2CLR|=127;
					*FIO2SET|=103;
					break;  //cambia flanco de bajada
				}
	}
	if(((*T1EMR>>1)&1)==1){
		switch (unidades){
				case 0 :
					*FIO2CLR|=127;
					*FIO2SET|=63;
					break;
				case 1 :
					*FIO2CLR|=127;
					*FIO2SET|=6;
					break;
				case 2 :
					*FIO2CLR|=127;
					*FIO2SET|=91;
					break;
				case 3 :
					*FIO2CLR|=127;
					*FIO2SET|=79;
					break;
				case 4 :
					*FIO2CLR|=127;
					*FIO2SET|=102;
					break;
				case 5 :
					*FIO2CLR|=127;
					*FIO2SET|=109;
					break;
				case 6 :
					*FIO2CLR|=127;
					*FIO2SET|=125;
					break;
				case 7 :
					*FIO2CLR|=127;
					*FIO2SET|=7;
					break;
				case 8 :
					*FIO2SET|=127;
					break;
				case 9 :
					*FIO2CLR|=127;
					*FIO2SET|=103;
					break;  //cambia flanco de bajada
				}
	}
	*T1IR|=1;				//MR0 interrupt flag for match channel 0
}

void TIMER0_IRQHandler(void){
	cuenta=*T0CR0;
	decenas=0;
	unidades=0;
	cuenta=cuenta&127;
	if((cuenta-100)>0){
		cuenta=cuenta-100;
	}
	while(cuenta>=10){
		cuenta=cuenta-10;
		decenas++;
	}
	unidades=cuenta;
	*T0IR|=(1<<4);				//MR0 interrupt flag for match channel 0
}
