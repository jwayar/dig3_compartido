//---------GPIO2---------------
#define AddrFIO2DIR 0X2009C040
#define AddrFIO2SET 0X2009C058
#define AddrFIO2CLR 0x2009C05C
#define AddrFIO2PIN 0x2009C054

#define AddrPCONP 0X400FC0C4
#define AddrPCLKSEL1 0x400FC1AC
#define AddrPINSEL3 0X4002C00C

//------TIMER3-----------
#define AddrT3EMR 0X4009403C
#define AddrT3MCR 0x40094014
#define AddrT3MR0 0X40094018
#define AddrISER0 0XE000E100
#define AddrT3TCR 0X40094004
#define AddrT3IR 0X40094000
#define AddrT3PR 0X4009400C

unsigned int volatile *const FIO2DIR= (unsigned int*) AddrFIO2DIR;
unsigned int volatile *const FIO2SET= (unsigned int*) AddrFIO2SET;
unsigned int volatile *const FIO2CLR= (unsigned int*) AddrFIO2CLR;
unsigned int volatile *const FIO2PIN= (unsigned int*) AddrFIO2PIN;

unsigned int volatile *const PCONP=(unsigned int*)AddrPCONP;
unsigned int volatile *const PCLKSEL1=(unsigned int*)AddrPCLKSEL1;
unsigned int volatile *const PINSEL3=(unsigned int*)AddrPINSEL3;

unsigned int volatile *const T3EMR=(unsigned int*)AddrT3EMR;
unsigned int volatile *const T3MCR=(unsigned int*)AddrT3MCR;
unsigned int volatile *const T3MR0=(unsigned int*)AddrT3MR0;
unsigned int volatile *const ISER0=(unsigned int*)AddrISER0;
unsigned int volatile *const T3TCR=(unsigned int*)AddrT3TCR;
unsigned int volatile *const T3IR=(unsigned int*)AddrT3IR;
unsigned int volatile *const T3PR=(unsigned int*)AddrT3PR;

int display=1;
int valor1,valor2,valor3,valor4;
void muestra(int);
int main(void){
	*FIO2DIR |=127;
	*FIO2DIR|=(29<<8);

	valor1=1;
	valor2=2;
	valor3=3;
	valor4=4;

	*PCONP|=(1<<23);		//enciendo el periferico del timer
	*PCLKSEL1|=(1<<14);	//peripheral clock: system clock
	*T3MCR|=3;			//reset on MR0 the TC will be reset if MR0 matches it. interrup on MR0: an interrup is
							//generated when MR0 matches the value in the TC
	*T3MR0=10000;		//match register 0 6450
	*ISER0|=(1<<4);
	*T3TCR|=1;			//habilito el registro del control del timer
	while(1){

	}
	return 0;
}

void TIMER3_IRQHandler(void){		//P2.0 a P2.7 Segmentos-- P2.8(sin P2.9) a P2.12 multiplexado
	display=(display<<1);
	if(display==16){
		display=1;
	}
	if (display==1){
		*FIO2CLR|=(1<<12);
		*FIO2SET|=(1<<8);
		muestra(valor1);
	}
	if(display==2){
		*FIO2CLR|=(1<<8);
		*FIO2SET|=(1<<10);
		muestra(valor2);
	}
	if(display==4){
		*FIO2CLR|=(1<<10);
		*FIO2SET|=(1<<11);
		muestra(valor3);
	}
	if(display==8){
		*FIO2CLR|=(1<<11);
		*FIO2SET|=(1<<12);
		muestra(valor4);
	}
	*T3IR|=1;				//MR0 interrupt flag for match channel 0
}

void muestra(int numero){				//valor del display en logica negativa
			switch (numero){			//modificar los valores que se envian
					case 0 :
						*FIO2SET|=127;
						*FIO2CLR|=63;
						break;
					case 1 :
						*FIO2SET|=127;
						*FIO2CLR|=6;
						break;
					case 2 :
						*FIO2SET|=127;
						*FIO2CLR|=91;
						break;
					case 3 :
						*FIO2SET|=127;
						*FIO2CLR|=79;
						break;
					case 4 :
						*FIO2SET|=127;
						*FIO2CLR|=102;
						break;
					case 5 :
						*FIO2SET|=127;
						*FIO2CLR|=109;
						break;
					case 6 :
						*FIO2SET|=127;
						*FIO2CLR|=125;
						break;
					case 7 :
						*FIO2SET|=127;
						*FIO2CLR|=7;
						break;
					case 8 :
						*FIO2CLR|=127;
						break;
					case 9 :
						*FIO2SET|=127;
						*FIO2CLR|=103;
						break;
}}
