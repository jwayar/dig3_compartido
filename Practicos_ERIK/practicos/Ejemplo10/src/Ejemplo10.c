//posiciones de memoria de los registros que configuran al GPIO0
#define AddrFIO0DIR 0x2009C000
#define AddrFIO0CLR 0x2009C01C
#define AddrFIO0SET 0x2009C018
//---------------------------
//declaracion de punteros a direcciones fijas
unsigned int volatile *const FIO0DIR=(unsigned int*)AddrFIO0DIR;
unsigned int volatile *const FIO0CLR=(unsigned int*)AddrFIO0CLR;
unsigned int volatile *const FIO0SET=(unsigned int*)AddrFIO0SET;
//---------------------------
//prototipo de funciones
extern void esperaEnAsm(int);
//---------------------------
//programa principal
int main(void){
	int retardo=100000;

	*FIO0DIR|=(1<<22); //define al puerto P0.22 como salida
	while(1){
		*FIO0SET|=(1<<22);// led off
		esperaEnAsm(retardo);	//llamada a subrutina de espera
		*FIO0CLR|=(1<<22);		//led off
		esperaEnAsm(retardo);	//llamada a subrutina de espera
	}
	return 0;
}
