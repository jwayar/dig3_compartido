.syntax unified //permite escribir instrucciones de 16 y 32 bits utilizando la misma sintaxis
;.data
.global esperaEnAsm //la funcion puede ser llamada por
					//cualquier funcion dentro del proyecto
.type esperaEnAsm, function

esperaEnAsm:
	SUBS R0,R0,#1; //DECREMENTA EN 1 EL VALOR DE R0
	BNE esperaEnAsm;//VERIFICA EL VALOR DEL REGISTRO ES 0
	MOV PC,LR;		//CARGA EN EL PC LA DIRECCION DE RETORNO
.END
