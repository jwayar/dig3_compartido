//////////////////////////////////////////////////////////////////////////////////
//                                                      VsZeNeR'05              
//                                                 22/Agosto/05
//                                                      vszener@gmail.com
//      Programa:       Contador 0-9 display BDC & Boton
//      Version:        0.0
//
//      Dispositivo: PIC 16F648A                        Compilador:      CCS vs3.227
//      Entorno IDE: MPLAB IDE v7.20            Simulador:       Proteus 6.7sp3
//
//      Notas: Contador 0 al 9 cada vez que pulsemos el boton y vuelta a empezar. Tener
//                 en cuenta que hay que poner la directiva NOLVP para que el pin B4 sea de 
//                 salida. Cuando agregamos un boton a nuestro circuito hay que tener en cuenta
//                 que este dispositivo genera 'rebotes' que hay que ser eliminados para
//                 una correcta visualizacion en el display del digito seleccionado. Esta vez
//                 la eliminacion de 'los rebotes' se ha realizado mediante software. 
//                 Cuando por el pin A0 del porta se introduce un '0' logico(low), se 
//                 incrementa un digito en el display BCD.
//
//      Conexiones:             A0 -> boton                   
//                                      B0 -> a 
//                                      B1 -> b 
//                                      B2 -> c 
//                                      B3 -> d 
//      BCD:
//                              d c b a   NUM
//                              0 0 0 0    0
//                              0 0 0 1    1
//                              0 0 1 0    2
//                              0 0 1 1    3
//                              0 1 0 0    4
//                              0 1 0 1    5
//                              0 1 1 0    6
//                              0 1 1 1    7
//                              1 0 0 0    8
//                              1 0 0 1    9
//////////////////////////////////////////////////////////////////////////////////
 
#include <16f648a.h>                            //pic a utilizar                        
#fuses XT,NOWDT,NOPROTECT,PUT,NOLVP             //ordenes para el programador 
#use delay (clock=4000000)                      //Fosc=4Mhz
#use fixed_io(b_outputs=PIN_B0,PIN_B1,PIN_B2,PIN_B3)
#use standard_io(A)
                
///PROGRAMA
void main(void)
{
        char i=0;       //contador para tabla BCD
        int tabBCD[10]={0b0000,0b0001,0b0010,0b0011,0b0100,0b0101,0b0110,0b0111,0b1000,0b1001}; //BCD 0-9
                
        set_tris_a(0xFF);                               //porta como entrada    
        disable_interrupts(GLOBAL);             //todas las interrupciones desactivadas
        
        output_b(tabBCD[i]);                    //inicializa displayBCD digito 0
 
        for(;;){                                                //bucle...
                if(!input(PIN_A0))                      //�se ha pulsado el boton?
                        {
                                delay_ms(151);          //SI -> retardo para evitar los rebotes
                                i++;                            //incremento contador indice tabBCD
                                if(i>9)                         //�se ha mostrado digito 9?
                                        i=0;                    //SI -> restaura valor indice(para mostrar digito 0)
                                output_b(tabBCD[i]); //muestra por portb digito 7 segmentos
                        }                       
                }                                                       //...infinito
}
 