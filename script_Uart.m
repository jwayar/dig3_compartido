%------------------------------------------------
% ------------- SERIAL COMUNICATION
%-------------------------------------------------

clear all; close all; clc; % Se limpia todo
delete( instrfind( {'Port'},{'COM5'} ) ); % Chequear el puerto: COM3 , COM4, COM5
dataType = 'string';  % puede ser 'string' or 'number'
buff=14;
s=serial('COM5');
set(s, 'BaudRate' , 9600 );
set(s, 'Terminator' , 'CR/LF' );
set(s, 'InputBufferSize' , buff);

fopen(s);

if strcmp( dataType, 'string' )
	charTx = input('Ingrese el caracter de eco:' , 's');
	fprintf(s, charTx); % Write text to device
	salida = fscanf(s); % Read ASCII data from device, and format as text
	display(salida)
elseif strcmp(dataType, 'number')
	numTx = input('Ingrese el numero de eco:' , 's');
	fprintf(s, numTx); % Write text to device
	salida = fread(s); % Read binary data from device
	plot(salida); 
	grid on
end

fclose(s)
delete(s)
clear s