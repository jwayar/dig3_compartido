/*
===============================================================================
 Name        : clase5_Ejemplo3.c
 Author      : $JWayar
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

/* ----------------------------------------------
 * @Output:	P2[6][5][4][3][2][1][0] : En ése orden!
 *  		"g,f,e,d,c,b,a" Display cátodo commún
 *			pin48 al pin42 (respetando el orden)
 *			falta display2
 * 			-------------------------------------
 * ----------------------------------------------
 * @Input:	P2[10]  (Pulsador EINT0->EINT1) pin 51
 * @Input:  P2[11]  (Pulsador EINT1->EINT0) pin 52
 * 			-------------------------------------
 * 1.- Modificar el ejemplo que se adjunta para que
 * mediante el sysTick timer se realice un multiplexado
 * de tiempo variable de dos displays de 7 segmentos.
 * Inicialmente el tiempo de multiplexado
 * será de 50 milisegundos.
 * El sistema contará con un pulsador que permitirá
 * incrementar este tiempo hasta un máximo de 95 ms
 * y un segundo pulsador que permitirá disminuirlo
 * hasta un mínimo de 5 ms.
 * Estos pulsadores deben estar conectados con
 * resistencias de pull-down internas y
 * deben actuar sobre las interrupciones externas
 * activadas por flancos.
 * En los displays deben observarse
 * el valor del tiempo en ms
 * a la que se está realizando la multiplexación actual.
 *
 */

#include "Ej10Wayar.h"

/*--------------------------------------------------
					CONFIGURACION
----------------------------------------------------*/

//-- Config Port
void config_GPIO(void){
	//-------------------------------------
	//--PINSEL (Select GPIO0 or other)
		/* by default 00 to all -> "GPIO0"
		 * PINSEL0 para P0[15:0]
		 * PINSEL1 para P0[31:16]
		 * PINSEL2 para P1[15:0]
		 * PINSEL3 para P1[31:16]
		 * PINSEL4 para P2[15:0]
		 */

	//-------------------------------------
	//--FIO2DIR (Select Input/Output)
		/* by default All Input
		 */
	// Output (Set 1):
	*FIO2DIR |= (
					(1<<Port2_Pin(0)) |
					(1<<Port2_Pin(1)) |
					(1<<Port2_Pin(2)) |
					(1<<Port2_Pin(3)) |
					(1<<Port2_Pin(4)) |
					(1<<Port2_Pin(5)) |
					(1<<Port2_Pin(6)) |
					(1<<Port2_Pin(7)) |
					(1<<Port2_Pin(8))
				);
	// Input (Set 0):
	*FIO2DIR &= ~(1<<Port2_Pin(10)); // P2[10]
	*FIO2DIR &= ~(1<<Port2_Pin(11)); //P2[11]

	//-------------------------------------
	//-- FI2MASK (enmascared to pins unused)
		/* Set 1: enmascarar pines no usados!
		 * Set 0: a los pines utilizados (In/Out)
		 */
	*FIO2MASK = ( 0xFFFF
						& ~(1<<Port2_Pin(0)) & ~(1<<Port2_Pin(1))
						& ~(1<<Port2_Pin(2)) & ~(1<<Port2_Pin(3))
						& ~(1<<Port2_Pin(4)) & ~(1<<Port2_Pin(5))
						& ~(1<<Port2_Pin(6)) & ~(1<<Port2_Pin(7))
						& ~(1<<Port2_Pin(8)) & ~(1<<Port2_Pin(10))
						& ~(1<<Port2_Pin(11))
				);
	//-------------------------------------
	//-- PINMODE
		/* PullDown (to INPUT) Enable in ConfigEint
		 * two Input: PULSADOR P2[10] P2[11]->PINMODE4
		 */
}

//-- Config EINT0
void config_EINT0(void){
	*PINSEL4 	|= (0x01<<20); // "01" para que P2[10] pase a la función1: EINT0
	//*PINMODE4	|= (0x11<<20); // "11" para que tenga Pull-Down activada
	*PINMODE4	&= ~ (0x11<<20); // cambiar a "11" para que tenga Pul-Down activada

	*EXTINT 	|= (0x1<<Pos_EINT(0)) ; // "1" para EXINT0, bajo bandera de EINT0

	*EXTMODE    |= (0x1<<Pos_EINT(0)) ; // Config EINT0 por Flanco
	*EXTPOLAR   |= (0x1<<Pos_EINT(0)) ; // Set 1: por Flanco de Subida
}

//-- Config EINT1
void config_EINT1(void){
	*PINSEL4 	|= (0x01<<22); // "01" para que P2[11] pase a la función1: EINT1
	*PINMODE4	|= (0x11<<22); // "11" para que tenga Pull-Down activada
	//*PINMODE4	&= ~ (0x11<<22); // cambiar a "11" para que tenga Pul-Down activada

	*EXTINT 	|= (0x1<<Pos_EINT(1)) ; // "1" para EXINT1, bajo bandera de EINT1

	*EXTMODE    |= (0x1<<Pos_EINT(1)) ; // Config EINT1 por Flanco
	*EXTPOLAR   |= (0x1<<Pos_EINT(1)) ; // Set 1: por Flanco de Subida
}

/*--------------------------------------------------
					INICIALIZACION
----------------------------------------------------*/
//-- Init Output
void init(void){
	unsigned int select_disp = 0 ;

	// para probar cada segmento "a,b,c,d....g"
	for(unsigned int pin=0; pin<7; pin++){
		*FIO2PIN = (1<<pin);

		for(unsigned multiplicador=0; multiplicador<2; multiplicador++){
			select_disp = (select_disp + 1) % 2 ; // para seleccionar entre unidad y decena
			if(select_disp){
				*FIO2CLR |= (1<<Port2_Pin(8)) ; // Disable Display Decenas
				*FIO2SET |= (1<<Port2_Pin(7)) ; // Enable Display Unidades
				delay_ms(500); // tick_ms = 10  -> 10 mili segundo
			}
			else{
				*FIO2CLR |= (1<<Port2_Pin(7)) ; // Disable Display Unidades
				*FIO2SET |= (1<<Port2_Pin(8)); // Enable Display Decenas
				delay_ms(500); // tick_ms = 10  -> 10 mili segundo
			}
		}
	}

	// para probar cada número "0,1,2...,9"
	for(unsigned int num=0; num<10; num++){
		display(num);

		for(unsigned multiplicador=0; multiplicador<100; multiplicador++){
			select_disp = (select_disp + 1) % 2 ; // para seleccionar entre unidad y decena
			if(select_disp){
				*FIO2CLR |= (1<<Port2_Pin(7)) ; // Disable Display Decenas
				*FIO2SET |= (1<<Port2_Pin(8)) ; // Enable Display Unidades
				delay_ms(10); // tick_ms = 10  -> 10 mili segundo
			}
			else{
				*FIO2CLR |= (1<<Port2_Pin(8)) ; // Disable Display Decenas
				*FIO2PIN |= (1<<Port2_Pin(7)); // Enable Display Decenas
				delay_ms(10); // tick_ms = 10  -> 10 mili segundo
			}
		}
	}

}

/*--------------------------------------------------
					METODOS
----------------------------------------------------*/
//-- Display
void display(unsigned int decimal){
	if(decimal<10 )
	{
		*FIO2PIN = dec_to_7seg_cc[decimal];
	}
}

// retardo
void delay_ms(unsigned int tick_ms)
{
	SysTickCnt=0;
	*STCTRL |=1;  // Habilita el contador de SysTick
	while(SysTickCnt < tick_ms); // aqui se queda (se va a la subrutina y regresa)
	*STCTRL |=0;  // Deshabilita el contador de SysTick
}
// Antirebote:
int debounce(int SampleA){
	//-- Set static variables:
	static int SampleB=0;
	static int SampleC=0;
	static int LastDebounceResult = 0;

	//-- Logical Function:
	LastDebounceResult = (LastDebounceResult &&
						(SampleA || SampleB || SampleC)) ||
						(SampleA && SampleB && SampleC);
	//-- Update Sample
	SampleC=SampleB;
	SampleB=SampleA;

	return LastDebounceResult;
}

/*--------------------------------------------------
					SUBRUTINAS
----------------------------------------------------*/
void EINT0_IRQHandler(void){
	//--- Hubo Interrupción por P2.10!
	// Hacer algo -> incrementar ticks
	static int Puls,PulsAnt;
	//Antirebote
	for(unsigned int j=0; j<3; j++)
	{
		//delay_ms(80); // retardo(50000);
		for(int p=0;p<20000;p++);
		PulsAnt= !(*FIO2PIN & (1<<Port2_Pin(10)));
		Puls=debounce(PulsAnt);
	}
	if(Puls==0 && PulsAnt==0)
	{
		// Aumentar Tiempo, hasta un valor máximo (95 mseg)

		if(tick_ms < 95)
			tick_ms += 5 ;
		else
			tick_ms = 95 ;
	}

	// Clear Interrupt Flag EINT0
	*EXTINT |= (1<<Pos_EINT(0));
}

void EINT1_IRQHandler(void){
	//--- Hubo Interrupción por P2.11!
	// Hacer algo -> decrementar ticks

	static int Puls,PulsAnt;
	//Antirebote
	for(unsigned int j=0; j<3; j++)
	{
		//delay_ms(80); // retardo(50000);
		for(int p=0;p<20000;p++);
		PulsAnt= !(*FIO2PIN & (1<<Port2_Pin(11)));
		Puls=debounce(PulsAnt);
	}
	if(Puls==0 && PulsAnt==0)
	{
		// Disminuir Tiempo, hasta un valor mínimo
		if(tick_ms > 5 )
			tick_ms -=5 ;
		else
			tick_ms= 5;
	}

	// Clear Interrupt Flag EINT1
	*EXTINT |= (1<<Pos_EINT(1));
}

void SysTick_Handler(void){
	SysTickCnt++;
}

/****************************************************
					MAIN
*****************************************************/
int main(void){

	//--- Parameters:
	unsigned int time = 1;  		// Define la unidad de tiempo del systick_ms (actual: en mseg)
	unsigned int cclk = 4000000; 	// Frecuencia de funcionamiento del reloj del CPU (4 MHz)
	unsigned int unidad, decena ;
	unsigned int EnableUnidad = 0;
	*STCTRL   |= (1<<2); 			 // Define la fuente de reloj del SysTick
	*STRELOAD = (cclk/1000)*time-1 ; // Define el valor de recarga del temporizador del SysTick
	*STCTRL  |= (1<<1);    			 // Habilita la interrupcion del SysTick

	// Configuración:
	config_GPIO();
	config_EINT0();
	config_EINT1();

	init();

	//NVIC_EnableIRQ(EINT0_IRQn);
	*ISER0 |= (1<<18); // Set 1 en la Pos 18 del ISER0 -> Enable EINT0
	//NVIC_EnableIRQ(EINT1_IRQn);
	*ISER0 |= (1<<19); // Set 1 en la Pos 19 del ISER0 -> Enable EINT1

	tick_ms=50;
	while(1){

		if(tick_ms > 9)
			decena=tick_ms/10;
		else
			decena = 0 ;

		unidad = tick_ms - decena*10;

		if(EnableUnidad){
			*FIO2CLR|= (1<<Port2_Pin(8));
			//display(unidad);
			*FIO2PIN = dec_to_7seg_cc[unidad];
			*FIO2SET|= (1<<Port2_Pin(7));
			delay_ms(tick_ms);
			EnableUnidad=!EnableUnidad;
		}
		else{
			*FIO2CLR|= (1<<Port2_Pin(7));
			//display(decena);
			*FIO2PIN = dec_to_7seg_cc[decena];
			*FIO2SET|= (1<<Port2_Pin(8));
			delay_ms(tick_ms);
			EnableUnidad=!EnableUnidad;
		}
	}
	return 0;
}
