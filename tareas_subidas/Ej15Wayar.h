/*
 * clase7_ejercicio15.h
 *
 *  Created on: 15 de oct. de 2018
 *      Author: joel
 */

#ifndef CLASE7_EJERCICIO15_H_
#define CLASE7_EJERCICIO15_H_

//-- To Led-P0[22] as Output
#define AddrFIO0DIR 	0x2009C000
#define AddrFIO0SET 	0x2009C018
#define AddrFIO0CLR 	0x2009C01C

//-- TO ADC P0[26]
#define AddrPCONP 		0x400FC0C4
#define AddrAD0CR 		0x40034000
#define AddrPINMODE1 	0x4002C044
#define AddrPINSEL1 	0X4002C004
#define AddrAD0INTEN	0x4003400C
#define AddrAD0DR0 		0x40034010
#define AddrPCLKSEL0 	0X400FC1A8
#define AddrAD0GDR 		0x40034004
#define AddrAD0STAT     0x40034030

#define AddrAD0DR1 		0x40034014

//-- To Timer1
//#define AddrPCONP 		0X400FC0C4
//#define AddrPCLKSEL0 	0X400FC1A8
//#define AddrPINSEL3 	0X4002C00C
//#define AddrPINMODE3 	0X4002C04C
#define AddrT1EMR 		0X4000803C
#define AddrT1MCR 		0X40008014
#define AddrT1MR0 		0X40008018
#define AddrISER0 		0XE000E100
#define AddrT1TCR 		0X40008004
#define AddrT1IR 		0X40008000
#define AddrT1PR 		0X4000800C

//-- Port2
#define AddrFIO2DIR	 0x2009C040
#define AddrFIO2MASK 0X2009C050
#define AddrFIO2PIN  0X2009C054
#define AddrFIO2SET  0X2009C058
#define AddrFIO2CLR  0X2009C05C

/*
 * All Interrupts
 * */
#define AddrISER0 		0XE000E100

// LED22
unsigned int volatile *const FIO0DIR 	= (unsigned int*) AddrFIO0DIR;
unsigned int volatile *const FIO0SET 	= (unsigned int*) AddrFIO0SET;
unsigned int volatile *const FIO0CLR 	= (unsigned int*) AddrFIO0CLR;

// ADC
unsigned int volatile *const PCONP 	 	= (unsigned int*) AddrPCONP;
unsigned int volatile *const AD0CR   	= (unsigned int*) AddrAD0CR;
unsigned int volatile *const AD0INTEN 	= (unsigned int*) AddrAD0INTEN;
unsigned int volatile *const PINMODE1 	= (unsigned int*) AddrPINMODE1;
unsigned int volatile *const PINSEL1    = (unsigned int*) AddrPINSEL1;
unsigned int volatile *const AD0DR0 	= (unsigned int*) AddrAD0DR0;
unsigned int volatile *const PCLKSEL0 	= (unsigned int*) AddrPCLKSEL0;
unsigned int volatile *const AD0GDR 	= (unsigned int*) AddrAD0GDR;
unsigned int volatile *const AD0STAT    = (unsigned int*) AddrAD0STAT ;
unsigned int volatile *const AD0DR1 	= (unsigned int*) AddrAD0DR1;

// TIMER0
//unsigned int volatile *const PCONP       = (unsigned int*) AddrPCONP;
//unsigned int volatile *const PCLKSEL0    = (unsigned int*) AddrPCLKSEL0;
//unsigned int volatile *const PINSEL3     = (unsigned int*) AddrPINSEL3;
//unsigned int volatile *const PINMODE3    = (unsigned int*) AddrPINMODE3;
unsigned int volatile *const T1EMR       = (unsigned int*) AddrT1EMR;
unsigned int volatile *const T1MCR       = (unsigned int*) AddrT1MCR;
unsigned int volatile *const T1MR0       = (unsigned int*) AddrT1MR0;
unsigned int volatile *const T1TCR       = (unsigned int*) AddrT1TCR;
unsigned int volatile *const T1IR        = (unsigned int*) AddrT1IR;
unsigned int volatile *const T1PR        = (unsigned int*) AddrT1PR;

// PORT-2
unsigned int volatile *const FIO2DIR 		= (unsigned int*) AddrFIO2DIR;
unsigned int volatile *const FIO2MASK		= (unsigned int*) AddrFIO2MASK;
unsigned int volatile *const FIO2PIN 		= (unsigned int*) AddrFIO2PIN;
unsigned int volatile *const FIO2SET 		= (unsigned int*) AddrFIO2SET;
unsigned int volatile *const FIO2CLR 		= (unsigned int*) AddrFIO2CLR;

// All Interrupts:
unsigned int volatile *const ISER0       = (unsigned int*) AddrISER0;

//unsigned int volatile *const name_puntero  = (unsigned int*) direccion;


/*-------------------------------------------
* -- Prototype Functions:
*-------------------------------------------*/
void config_ADC(void);
void config_GPIO(void);
void config_Timer1(void);

void enableInterrupts(void);

void ADC_IRQHandler(void);
void TIMER1_IRQHandler(void);

unsigned short int unidad=0,decima=0;

const char dec_to_7seg_cc[10]={  //abcdefg
                 0b1111110, //0
                 0b0110000, //1
                 0b1101101, //2
                 0b1111001, //3
                 0b0110011, //4
                 0b1011011, //5
                 0b1011111, //6
                 0b1110000, //7
                 0b1111111, //8
                 0b1110011  //9
                };


#endif /* CLASE7_EJERCICIO15_H_ */
