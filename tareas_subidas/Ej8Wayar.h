/*
===============================================================================
 Name        : clase4_ejercicio8.h
 Author      : $jwayar
 Version     :
 Copyright   : $FCEFyN-UNC
 Description : 6Leds+Pulsador c/ y s/ rebote
===============================================================================
*/

/* ----------------------------------------------
 * @Output:	P0[8][7][6][0][1][18] : En ése orden!
 * 			-------------------------------------
 * ----------------------------------------------
 * @Input:	P0[9]  (Pulsador)
 * 			-------------------------------------
 */
#define posLED0 8
#define posLED1 7
#define posLED2 6
#define posLED3 0
#define posLED4 1
#define posLED5 18
#define posPULS 9
/*-------------------------------------------
    Posiciones de memoria para config GPIO0
 *------------------------------------------*/
#define AddrFIO0DIR  0X2009C000
#define AddrFIO0SET  0X2009C018
#define AddrFIO0CLR  0X2009C01C
#define AddrFIO0PIN  0X2009C014
#define AddrFIO0MASK 0X2009C010
#define AddrPINMODE0 0X2009C040 // Config PinMode (to Pull-UP P0[9]) del GPIO0, pines del 0 al 15
/* Definir Puertos a las direcciones
 * de memoria por las correspondientes constantes:
 */
unsigned int volatile *const FIO0DIR  = (unsigned int*) AddrFIO0DIR;
unsigned int volatile *const FIO0SET  = (unsigned int*) AddrFIO0SET;
unsigned int volatile *const FIO0CLR  = (unsigned int*) AddrFIO0CLR;
unsigned int volatile *const FIO0PIN  = (unsigned int*) AddrFIO0PIN;
unsigned int volatile *const FIO0MASK = (unsigned int*) AddrFIO0MASK;
unsigned int volatile *const PINMODE0 = (unsigned int*) AddrPINMODE0;

// Prototipo de Funciones:
void configuracion(void ); 	//-- Config Port
void init(void );			//-- Init Output
int retardo(unsigned int ); //-- Function delay
int debounce(int );         //-- Function Debounce
