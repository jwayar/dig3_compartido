/*
===============================================================================
 Name        : clase7_ejercicio14.c
 Author      : jwayar
 Version     :
 Copyright   : FCEFyN
 Description : main definition
===============================================================================
*/

/* ---------------------------------------------------------
 *  @InPut:
 *	 	PO[23] as AD0[0]
 *      P2[10]  (Pulsador EINT0) pin 51
 *
 *	@OutPut:
 *	    P1[29] Match0.1 PINSEL3 [26:27] , PAD[12] de la placa
 *		P0[22]: Led_OFF-ON: de acuerdo a la lectura del conversor, si
 *							supera o no la mitad de la Vref
 *
 * Conexión:
 * -------------------------------------------
 *	Pin28 V+ ----/\/\/\/\/\---- GND
 *					  |
 *		              |
 *		           P0[23] "AD0.0"
 *---------------------------------------------
 * (in)	P2[10] <- P1[29]  (output without Pull-Up/ Pull-Down)
 * --------------------------------------------
 */

/*
 * Modificar el ejemplo del ADC para que
 * el inicio de conversión sea realizado por el flanco ascendente del MAT0.1
 *
 *  MR0[1]: External Match Output  (P1[29])
 *  	Cuando el MR0[1] sea igual al TC la salida puede
 *  		toglear, Set 0 , Set 1 ó no hacer nada
 *  		(configurable mediante el External Match Register "EMR")
 *
 * "Se debe configurar el ADC con Burst=0" -> Software controlled mode
 * [Bit 24:26] – START
 * When the BURST [16 bit] is 0, these bits control whether and when an A/D conversion is started:
 *  000 - Conversion Stopped
 *  001- Start Conversion Now
 * */

#include "Ej14Wayar.h"

#define Pos_EINT(x)  x

/*--------------------------------------------------
          CONFIGURACION
----------------------------------------------------*/
//-- Config GPIO
void config_GPIO(void){
	*FIO0DIR |= (1<<22);
}

//-- Config ADC Mode Burst
void config_ADC(void){

	/*_1. POWER: ---------------------------------------------
		In the PCONP register (Table 46) */
	*PCONP |= (1<<12); // Power Control for Peripherals Register.
	*AD0CR |= (1<<21); // The A-D converter is operational.

	/*2. Clock: ----------------------------------------------
	In the PCLKSEL0 register (Table 40), select PCLK_ADC.
	 To scale the clock for the ADC, see bits CLKDIV in Table 532. */
	*PCLKSEL0 |= (3<<24); // "00" -> CCLK/"4"
	*AD0CR &=~ (255<<8); // CLK_DIV = 255
		/*
			The APB clock (PCLK_ADC0) is divided by (this value plus one)
			to produce the clock for the A-D converter,
			wich should be less than or equal to 13MHz.
		*/

	*AD0CR &=~ (1<<16); // *ADOCR[16] = 0 -> BURST=0
		/*
			Software controlled mode:
			para iniciar conversion -> *AD0CR |= (1<<24)
		*/

	/* 3_ Pins: ------------------------------------------
		Enable ADC0 pins through PINSEL registers.
		Select the pin modes for the port pins with ADC0 functions
		through the PINMODE registers (Section 8.5).
	*/
	*PINSEL1  |= (1<<14); // PO[23] as AD0[0]
	*PINMODE1 |= (1<<15); // PO[23] Pin has neither PullUp nor PullDown resistor enabled.
	*AD0CR    |= (1<<0);        /* Select Channel 0 by setting 0th bit of ADCR */
	/*	4_ Interrupts: ----------------------------------
		To enable interrupts in te ADC, see Table 536. Interrupts are enabled
		in the NVIC using the appropriate Interrupt Set Enable register.
	*/
	*AD0INTEN = 1 ;
		/* 				>AD0INTEN<
			This register allows control over wich A-D channels generate an interrupt
			when a conversion complete.

			Completaton of a conversion on ADC channel 0 will not generate an interrupt.
		*/

}

//-- Config TIMER0
void config_Timer0(void){
	// ------------------------
	// Enable Power for Timer0:
	// ------------------------
    *PCONP    |= (0x1<<1);

    // -----------------------------------
    // Disable counter and hold in reset:
    // -----------------------------------
    *T0TCR = 2;       // rest counter (1<<1)

    // -------------------------------------------
    // Set Clock source for Timer0 (bit 12 and 13)
    // we pick full system clock, divider 2,4,8
    // are also available,
    // For Timer0 bits[3:2] in PCLKSEL0 are used.
    //  [10] – PCLK = CCLK/2
    // -------------------------------------------
    *PCLKSEL0 |= (0x2<<2);   // Peripheral clock: cclk/2

    // ----------------------------------
    // Use normal Timer mode, no capture
    // ----------------------------------
    // *CTCR0 = 0;  // by default

    // -----------------------------------------------
    // Match on MR1 = TC.
    // -----------------------------------------------
    *T0MCR    = (1<<4) ;    // Reset on MR1, no interrupts (disable by default)

    // ------------------------------------------------
    // set pin function for pin PAD_12 Match0.1 P1[29]
    // ------------------------------------------------
    *PINSEL3  |= (0x3<<26); // Pins: Match0.1 P1[29]

    // ----------------------------------
    // toggle pin PAD_12 Match0.1 P1[29] on match
    // ----------------------------------
    *T0EMR   = (1<<1)|(1<<6)|(1<<7); // |= 1 | (0x3<<6);
    	// *T0EMR    |= (0x2<<6);   // -> Set the corresponding External Match output to 1 (MATx.m pin is HIGH).

    // -----------------------------------
    // Set clock divider and match value
    // this determines the final frequency
    // -----------------------------------
    *T0PR=0;
    *T0MR1    = 10000000 ;     // Match Register 1 del Timer0 (aprox 5 seg)

    // *ISER0 |= (1<<1); sin interrupt
    // start counter
    *T0TCR    = 1  ;
}

//-- Config EINT0
void config_EINT0(void){
	//Input by default
	
	*PINSEL4 	|= (0x01<<20); 	 // "01" para que P2[10] pase a la función1: EINT0

	*PINMODE4	|= (0x1<<20);  // Pin has neither PullUp nor PullDown resistor enabled.

	*EXTINT 	|= (0x1<<Pos_EINT(0)) ; // "1" para EXINT0, bajo bandera de EINT0

	*EXTMODE    |= (0x1<<Pos_EINT(0)) ; // Config EINT0 por Flanco
	*EXTPOLAR   |= (0x1<<Pos_EINT(0)) ; // Set 1: por Flanco de Subida
}
/*--------------------------------------------------
          METODOS
----------------------------------------------------*/
void enableInterrupts(void){
	*ISER0 |= (1<<22);  // ADC 	  Interrupt Enable
	*ISER0 |= (1<<18);  // EINT0  Interrupt Enable
}

/*--------------------------------------------------
          SUBRUTINAS
----------------------------------------------------*/
//--ADC
void ADC_IRQHandler(void){
	unsigned short volatile ADC0Value = 0;
	float volts = 0;
	ADC0Value = ( *AD0DR0 >> 4 ) & 0xFFF; // *AD0DR0 contains the most recent conversion

	if ( ADC0Value < 2047)
		*FIO0SET |= (1<<22); // Vadc < Vref/2 -> Prendo Led22
	else
		*FIO0CLR |= (1<<22); // Vadc > Vref/2 -> Apago Led22

	//volts = (ADC0Value*VREF)/4096.0; //Convert result to Voltage
	//printf("AD0.0 = %dmV\n" , (int)(volts*1000)); //Display milli-volts
}
// EINT0-P2[10]
void EINT0_IRQHandler(void){
	*AD0CR |= (1<<24);
	// Clear Interrupt Flag EINT0
	*EXTINT |= (1<<Pos_EINT(0));
}

/****************************************************
          MAIN
*****************************************************/
int main (void){

	config_ADC();
	config_GPIO();
	config_Timer0();
	config_EINT0();
	enableInterrupts();
	while(1){

	}

	return 0;
}
