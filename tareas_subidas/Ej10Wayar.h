/*
 * clase5_ejercicio10.h
 *
 *  Created on: 2 de oct. de 2018
 *      Author: joel
 */

#ifndef CLASE5_EJERCICIO10_H_
#define CLASE5_EJERCICIO10_H_

#define AddrPINSEL0  0x4002C000
#define AddrPINSEL1  0x4002C004
#define AddrPINSEL2  0x4002C008
#define AddrPINSEL3  0x4002C00C
#define AddrPINSEL4  0x4002C010
#define AddrPINMODE4 0x4002C050
/*--------------
 * PORT-0
 *--------------*/
#define AddrFIO0DIR	 0x2009C000
#define AddrFIO0MASK 0X2009C010
#define AddrFIO0PIN	 0x2009C014
#define AddrFIO0SET	 0x2009C018
#define AddrFIO0CLR	 0x2009C01C

// Registros Asociados a las
// Interrupciones por GPIO0 EINT3
#define AddrIO0IntEnR 	0x40028090
#define AddrIO0IntEnF 	0x40028094
#define AddrISER0     	0xE000E100
#define AddrIO0IntStatR 0x400028084
#define AddrIO0IntStatF 0x400028088
#define AddrIO0IntClr 	0x4002808C

/*--------------
 * PORT-2
 *--------------*/
#define AddrFIO2DIR	 0x2009C040
#define AddrFIO2MASK 0X2009C050
#define AddrFIO2PIN  0X2009C054
#define AddrFIO2SET  0X2009C058
#define AddrFIO2CLR  0X2009C05C

#define Port2_Pin(x)   x
// Port2_Pin10  // to Interrup
// Port2_Pin11  // to Interrup
#define Pos_EINT(x)  x

// Registros Asociados a las
// Interrupciones por GPIO2 EINT0,1
#define AddrEXTINT 		0x400FC140
#define AddrEXTMODE 	0x400FC148
#define AddrEXTPOLAR	0x400FC14C


/*-------------------------------------------
	Registros Asociados para config SysTick
*------------------------------------------*/
#define AddrSTCTRL  	0xE000E010
#define AddrSTRELOAD 	0XE000E014

unsigned int volatile *const PINSEL0 		= (unsigned int*) AddrPINSEL0;
unsigned int volatile *const PINSEL1 		= (unsigned int*) AddrPINSEL1;
unsigned int volatile *const PINSEL2 		= (unsigned int*) AddrPINSEL2;
unsigned int volatile *const PINSEL3 		= (unsigned int*) AddrPINSEL3;
unsigned int volatile *const PINSEL4 		= (unsigned int*) AddrPINSEL4;

unsigned int volatile *const PINMODE4       = (unsigned int*) AddrPINMODE4;

unsigned int volatile *const FIO0DIR 		= (unsigned int*) AddrFIO0DIR;
unsigned int volatile *const FIO0MASK		= (unsigned int*) AddrFIO0MASK;
unsigned int volatile *const FIO0PIN 		= (unsigned int*) AddrFIO0PIN;
unsigned int volatile *const FIO0SET 		= (unsigned int*) AddrFIO0SET;
unsigned int volatile *const FIO0CLR 		= (unsigned int*) AddrFIO0CLR;

unsigned int volatile *const FIO2DIR 		= (unsigned int*) AddrFIO2DIR;
unsigned int volatile *const FIO2MASK		= (unsigned int*) AddrFIO2MASK;
unsigned int volatile *const FIO2PIN 		= (unsigned int*) AddrFIO2PIN;
unsigned int volatile *const FIO2SET 		= (unsigned int*) AddrFIO2SET;
unsigned int volatile *const FIO2CLR 		= (unsigned int*) AddrFIO2CLR;

unsigned int volatile *const IO0IntEnR 		= (unsigned int*) AddrIO0IntEnR;
unsigned int volatile *const IO0IntEnF 		= (unsigned int*) AddrIO0IntEnF;
unsigned int volatile *const ISER0 			= (unsigned int*) AddrISER0;
unsigned int volatile *const IO0IntStatR 	= (unsigned int*) AddrIO0IntStatR;
unsigned int volatile *const IO0IntStatF 	= (unsigned int*) AddrIO0IntStatF;
unsigned int volatile *const IO0IntClr 		= (unsigned int*) AddrIO0IntClr;

unsigned int volatile *const EXTINT         = (unsigned int*) AddrEXTINT;
unsigned int volatile *const EXTMODE        = (unsigned int*) AddrEXTMODE;
unsigned int volatile *const EXTPOLAR       = (unsigned int*) AddrEXTPOLAR;

unsigned int volatile *const STCTRL   = (unsigned int*) AddrSTCTRL;
unsigned int volatile *const STRELOAD = (unsigned int*) AddrSTRELOAD;

/*-------------------------------------------
* -- Prototype Functions:
*-------------------------------------------*/
void config_GPIO(void);
void config_EINT0(void);
void config_EINT1(void);
void EINT0_IRQHandler(void );
void EINT1_IRQHandler(void );
void delay_ms(unsigned int );
void display(unsigned int);
void SysTick_Handler (void);

// global variables:
volatile unsigned int SysTickCnt; // Contador de Interrupciones
unsigned int tick_ms;    // tiempo de multiplexado contiene 2 digitos-> unidad y decena

// g[6], f[5],e[4],d[3],c[2],b[1],a[0]
const char dec_to_7seg_ac[10]={
								 0xC0, // el 0 en el Display
								 0xF9,
								 0xA4,
								 0xB0,
								 0x99,
								 0x92,
								 0x82,
								 0xB8,
								 0x80,
								 0x98
								};

// a[6],b[5],c[4],d[3],e[2],f[1],g[0]
//
const char dec_to_7seg_cc[10]={  //abcdefg
								 0b1111110, //0
								 0b0110000, //1
								 0b1101101, //2
								 0b1111001, //3
								 0b0110011, //4
								 0b1011011, //5
								 0b1011111, //6
								 0b1110000, //7
								 0b1111111, //8
								 0b1110011  //9
								};

// const char dec_to_7seg_cc[10]={
// 								 0x3F,
// 								 0x06,
// 								 0x5B,
// 								 0x4F,
// 								 0x66,
// 								 0x6D,
// 								 0x7D,
// 								 0x07,
// 								 0x7F,
// 								 0x6F
// 								};

#endif /* CLASE5_EJERCICIO10_H_ */

