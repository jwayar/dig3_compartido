/*
 * EJ13_WAYAR_H_
 *
 *  Created on: 11 de oct. de 2018
 *      Author: joel
 */

#ifndef EJ13_WAYAR_H_
#define EJ13_WAYAR_H_

//-- To Led-P0[22] as Output
#define AddrFIO0DIR 	0x2009C000
#define AddrFIO0SET 	0x2009C018
#define AddrFIO0CLR 	0x2009C01C

//-- TO ADC P0[26]
#define AddrPCONP 		0x400FC0C4
#define AddrAD0CR 		0x40034000
#define AddrAD0INTEN	0x4003400C
#define AddrPINMODE1 	0x4002C044
#define AddrPINSEL1 	0X4002C004
#define AddrAD0DR0 		0x40034010
#define AddrPCLKSEL0 	0X400FC1A8
#define AddrAD0GDR 		0x40034004

/*--------------
 * PORT-2 P2[10] EINT0
 *--------------*/
#define AddrFIO2DIR	 0x2009C040
#define AddrFIO2MASK 0X2009C050
#define AddrFIO2PIN  0X2009C054
#define AddrFIO2SET  0X2009C058
#define AddrFIO2CLR  0X2009C05C
#define AddrPINSEL4  0x4002C010
#define AddrPINMODE4 0x4002C050

/*-------------------------------------------
	 Registros Asociados a las
	 Interrupciones por GPIO2 EINT0,1
*------------------------------------------*/
#define AddrEXTINT 		0x400FC140
#define AddrEXTMODE 	0x400FC148
#define AddrEXTPOLAR	0x400FC14C

/*
 * All Interrupts
 * */
#define AddrISER0 		0XE000E100

// LED22
unsigned int volatile *const FIO0DIR 	= (unsigned int*) AddrFIO0DIR;
unsigned int volatile *const FIO0SET 	= (unsigned int*) AddrFIO0SET;
unsigned int volatile *const FIO0CLR 	= (unsigned int*) AddrFIO0CLR;

// PORT-2 P2[10] EINT0
unsigned int volatile *const FIO2DIR 		= (unsigned int*) AddrFIO2DIR;
unsigned int volatile *const FIO2MASK		= (unsigned int*) AddrFIO2MASK;
unsigned int volatile *const FIO2PIN 		= (unsigned int*) AddrFIO2PIN;
unsigned int volatile *const FIO2SET 		= (unsigned int*) AddrFIO2SET;
unsigned int volatile *const FIO2CLR 		= (unsigned int*) AddrFIO2CLR;

unsigned int volatile *const PINSEL4 		= (unsigned int*) AddrPINSEL4;
unsigned int volatile *const PINMODE4       = (unsigned int*) AddrPINMODE4;

// GPIO2 EINT0,1
unsigned int volatile *const EXTINT      = (unsigned int*) AddrEXTINT;
unsigned int volatile *const EXTMODE     = (unsigned int*) AddrEXTMODE;
unsigned int volatile *const EXTPOLAR    = (unsigned int*) AddrEXTPOLAR;

// ADC
unsigned int volatile *const PCONP 	 	= (unsigned int*) AddrPCONP;
unsigned int volatile *const AD0CR   	= (unsigned int*) AddrAD0CR;
unsigned int volatile *const AD0INTEN 	= (unsigned int*) AddrAD0INTEN;
unsigned int volatile *const PINMODE1 	= (unsigned int*) AddrPINMODE1;
unsigned int volatile *const PINSEL1    = (unsigned int*) AddrPINSEL1;
unsigned int volatile *const AD0DR0 	= (unsigned int*) AddrAD0DR0;
unsigned int volatile *const PCLKSEL0 	= (unsigned int*) AddrPCLKSEL0;
unsigned int volatile *const AD0GDR 	= (unsigned int*) AddrAD0GDR;

// Interrupts:
unsigned int volatile *const ISER0       = (unsigned int*) AddrISER0;

//unsigned int volatile *const name_puntero  = (unsigned int*) direccion;


/*-------------------------------------------
* -- Prototype Functions:
*-------------------------------------------*/
void configADC(void);
void configGPIO(void);
void config_EINT0(void);

int debounce(int );
void enableInterrupts(void);

void EINT0_IRQHandler(void);
void ADC_IRQHandler(void);


#endif /* EJ13_WAYAR_H_*/
