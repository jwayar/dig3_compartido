/*
 * Ej12Wayar.h
 *
 *  Created on: 4 de oct. de 2018
 *      Author: joel
 */

#ifndef EJ12WAYAR_H_
#define EJ12WAYAR_H_

//-- To Led-P0[22] as Output
#define AddrFIO0DIR	 0x2009C000
#define AddrFIO0SET	 0x2009C018
#define AddrFIO0CLR	 0x2009C01C

//-- To P1[26] CAP0.0
#define AddrFIO1DIR  	0x2009C020
#define AddrFIO1PIN  	0x2009C034

//-- To Display P2[]
#define AddrFIO2DIR 	0X2009C040
#define AddrFIO2SET 	0X2009C058
#define AddrFIO2CLR 	0x2009C05C
#define AddrFIO2PIN 	0x2009C054

#define AddrPINSEL0 	0X4002C000
#define AddrPINSEL3 	0X4002C00C

#define AddrPCONP 		0X400FC0C4
#define AddrPCLKSEL0 	0X400FC1A8
//-------TIMER0----------
#define AddrT0EMR 		0X4000403C
#define AddrT0MCR 		0X40004014
#define AddrT0MR0 		0X40004018
#define AddrT0TCR 		0X40004004
#define AddrT0IR 		0X40004000
#define AddrT0PR 		0X4000400C
#define AddrT0CCR 		0X40004028
#define AddrT0CR0 		0X4000402C
//------TIMER1-----------
#define AddrT1EMR 		0X4000803C
#define AddrT1MCR 		0X40008014
#define AddrT1MR0 		0X40008018
#define AddrISER0 		0XE000E100
#define AddrT1TCR 		0X40008004
#define AddrT1IR 		0X40008000
#define AddrT1PR 		0X4000800C

unsigned int volatile *const FIO1DIR = (unsigned int*) AddrFIO1DIR ;
unsigned int volatile *const FIO1PIN = (unsigned int*) AddrFIO1PIN ;

unsigned int volatile *const FIO2DIR  = (unsigned int*) AddrFIO2DIR;
unsigned int volatile *const FIO2SET  = (unsigned int*) AddrFIO2SET;
unsigned int volatile *const FIO2CLR  = (unsigned int*) AddrFIO2CLR;
unsigned int volatile *const FIO2PIN  = (unsigned int*) AddrFIO2PIN;

unsigned int volatile *const PINSEL0  = (unsigned int*) AddrPINSEL0;

unsigned int volatile *const FIO0DIR 		= (unsigned int*) AddrFIO0DIR;
unsigned int volatile *const FIO0SET 		= (unsigned int*) AddrFIO0SET;
unsigned int volatile *const FIO0CLR 		= (unsigned int*) AddrFIO0CLR;

unsigned int volatile *const PCONP    = (unsigned int*) AddrPCONP;
unsigned int volatile *const PCLKSEL0 = (unsigned int*) AddrPCLKSEL0;
unsigned int volatile *const PINSEL3  = (unsigned int*) AddrPINSEL3; // to P1[26]-> Modo Capture "CAP0.0"

unsigned int volatile *const T0EMR    = (unsigned int*) AddrT0EMR;
unsigned int volatile *const T0MCR    = (unsigned int*) AddrT0MCR;
unsigned int volatile *const T0MR0    = (unsigned int*) AddrT0MR0;
unsigned int volatile *const T0TCR    = (unsigned int*) AddrT0TCR;
unsigned int volatile *const T0IR     = (unsigned int*) AddrT0IR;
unsigned int volatile *const T0PR     = (unsigned int*) AddrT0PR;
unsigned int volatile *const T0CCR    = (unsigned int*) AddrT0CCR;
unsigned int volatile *const T0CR0    = (unsigned int*) AddrT0CR0;

unsigned int volatile *const T1EMR    = (unsigned int*) AddrT1EMR;
unsigned int volatile *const T1MCR    = (unsigned int*) AddrT1MCR;
unsigned int volatile *const T1MR0    = (unsigned int*) AddrT1MR0;
unsigned int volatile *const ISER0    = (unsigned int*) AddrISER0;
unsigned int volatile *const T1TCR    = (unsigned int*) AddrT1TCR;
unsigned int volatile *const T1IR     = (unsigned int*) AddrT1IR;
unsigned int volatile *const T1PR     = (unsigned int*) AddrT1PR;

unsigned int cuenta;
unsigned int decena,unidad;

void config_GPIO(void);
void config_TIMER0(void);
void config_TIMER1(void);
void display(unsigned int);
int debounce(int);

void TIMER1_IRQHandler(void);
void TIMER0_IRQHandler(void);

// a[6],b[5],c[4],d[3],e[2],f[1],g[0]
//
const char dec_to_7seg_cc[10]={  //abcdefg
                 0b1111110, //0
                 0b0110000, //1
                 0b1101101, //2
                 0b1111001, //3
                 0b0110011, //4
                 0b1011011, //5
                 0b1011111, //6
                 0b1110000, //7
                 0b1111111, //8
                 0b1110011  //9
                };

#endif EJ12WAYAR_H_