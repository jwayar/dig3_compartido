/*
===============================================================================
 Name        : Ej11_3Wayar.c
 Author      : $Jwayar
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#include "Ej11_3Wayar.h"

void configGPIO(void){
    *FIO0DIR |= (1<<22); // led de la placa como salida
}

void configTimer1(void){
    *PCONP    |= (0x1<<2);   // Enciendo el Periferico del Timer1
    *PCLKSEL0 |= (0x1<<4);   // Peripheral clock: system clock
    *PINSEL3  |= (0x3<<24); // Pins: Match0.0
    *T1EMR    |= (0x3<<4);  //
    *T1MCR    |= 0x3 ;      // Reset on MR0:
    // the TC will be reset if MR0 matches it
    // Interrupt on MR0: an interrupt is generated 
    // when MR0 matches the value in the TC
    *T1MR0    = 364500 ;     // Match Register 0
    *ISER0    |= (0x1<<2);
    *T1TCR    |= 0x1  ;      // Enable Control Register of Timer
}

void TIMER1_IRQHandler(void)
{
    static int i=0;
    if (i==0){
        *FIO0SET |= (1<<22);
        i=!i;
    }
    else if (i==1){
        *FIO0CLR |= (1<<22);
        i=!i;
    }
    *T1IR |=1 ; // MR0 Interrupt Flag Down for Match Channel 0
}
int main(void){
    configGPIO();
    *FIO0SET |= (1<<22);
    configTimer0();

    while(1){}

    return 0;

}
