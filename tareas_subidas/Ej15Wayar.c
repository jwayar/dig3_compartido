/*
===============================================================================
 Name        : clase7_Ejemplo5.c
 Author      : $jwayar
 Version     :
 Copyright   : $ FCEFyN
 Description : main definition
===============================================================================
*/

/* ---------------------------------------------------------
 *  @InPut:
 *	 	PO[23] as AD0[0]
 *
 *	@OutPut:
 *		P0[22]: Led_OFF-ON: de acuerdo a la lectura del conversor, si
 *							supera o no la mitad de la Vref
 *
 * Conexión1:
 *	Pin28 V+ ----/\/\/\/\/\---- GND
 *					  |
 *		              |
 *		           P0[23] "AD0.0"(in)
 *
 * Conexión2:
 *	Pin28 V+ ----/\/\/\/\/\---- GND
 *					  |
 *		              |
 *		           P0[24] (in) "AD0.1"
 *
 * Conexión3:  DISPLAY unidad, decena
 *
 *	Modificar el ejemplo del ADC para que:
 *	 mediante dos display de 7 segmentos se muestre el valor de tensión de
 * un segundo canal del ADC que se encuentra conectado a un potenciómetro.
 *
 * Adjuntar un video mostrando el funcionamiento de los dos potenciómetros,
 *  junto con el valor medido por un voltímetro
 *  conectado al potenciómetro que modifica el valor de los displays.
 */


#include "Ej15Wayar.h"
#define VREF 2.9
#define RESOLUCION 0.1
#define DIVISOR_ADC (4096*RESOLUCION)/(VREF)
#define Port2_Pin(x)   x

/*--------------------------------------------------
          CONFIGURACION
----------------------------------------------------*/
//-- Config GPIO
void config_GPIO(void){
	*FIO0DIR |= (1<<22);

	//-------------------------------------
	//--FIO2DIR (Select Input/Output)
		/* by default All Input
		 */
	// Output (Set 1):
	*FIO2DIR |= (
					(1<<Port2_Pin(0)) |
					(1<<Port2_Pin(1)) |
					(1<<Port2_Pin(2)) |
					(1<<Port2_Pin(3)) |
					(1<<Port2_Pin(4)) |
					(1<<Port2_Pin(5)) |
					(1<<Port2_Pin(6)) |
					(1<<Port2_Pin(7)) |
					(1<<Port2_Pin(8))
				);
}

//-- Config ADC
void config_ADC(void){

	/*_1. POWER: ---------------------------------------------
		In the PCONP register (Table 46) */
	*PCONP |= (1<<12); // Power Control for Peripherals Register.
	*AD0CR |= (1<<21); // The A-D converter is operational.

	/*2. Clock: ----------------------------------------------
	In the PCLKSEL0 register (Table 40), select PCLK_ADC.
	 To scale the clock for the ADC, see bits CLKDIV in Table 532. */
	*PCLKSEL0 |= (3<<24); // "00" -> CCLK/"4"
	*AD0CR &=~ (255<<8);
		/*
			The APB clock (PCLK_ADC0) is divided by (this value plus one)
			to produce the clock for the A-D converter,
			wich should be less than or equal to 13MHz.
		*/

	*AD0CR |= (1<<16); // *ADOCR[16] = 1  modo conversiones repetittivas escaneando los pines seleccionados en "SEL"
		/*                   *AD0CR |= (1<<16)
			The AD converter does repeated conversions at up to 200Khz, scanning (if necessary)
			througth the pins selected by bits set to ones in the SEL field.
		*/

	/* 3_ Pins: ------------------------------------------
		Enable ADC0 pins through PINSEL registers.
		Select the pin modes for the port pins with ADC0 functions
		through the PINMODE registers (Section 8.5).
	*/
	*PINSEL1  |= (1<<14); // PO[23] as AD0[0]
	*PINMODE1 |= (1<<15); // PO[23] Pin has neither PullUp nor PullDown resistor enabled.
	*PINSEL1  |= (1<<16); // PO[24] as AD0[0]
	*PINMODE1 |= (1<<17); // PO[24] Pin has neither PullUp nor PullDown resistor enabled.

	*AD0CR |= (1<<0); // AD0.0 seleccionado
	*AD0CR |= (1<<1); // AD0.1 seleccionado

	/*	4_ Interrupts: ----------------------------------
		To enable interrupts in te ADC, see Table 536. Interrupts are enabled
		in the NVIC using the appropriate Interrupt Set Enable register.
	*/
	* AD0INTEN = 3 ;
		/* 				>AD0INTEN<
			This register allows control over wich A-D channels generate an interrupt
			when a conversion complete.

			Completaton of a conversion on ADC channel 0 will not generate an interrupt.
		*/

}

//-- Config Timer1
void config_TIMER1(void){
  // Config Timer1: Mode Match (Multiplexado 20mseg)
  *PCONP|=(3<<1);   //enciendo el periferico del timer
  *PCLKSEL0|=(3<<4);  //peripheral clock: system clock Timer1 cclk/8

  *T1EMR|=(15<<4);
  *T1EMR|=1;
  *T1EMR&=~(1<<1);
  *T1MCR|=3;      //reset on MR0 the TC will be reset if MR0 matches it. interrup on MR0: an interrup is
                  //generated when MR0 matches the value in the TC
  *T1MR0=4000;    //match register 0 6450

  *T1TCR|=1;      //habilito el registro del control del timer
}
/*--------------------------------------------------
          METODOS
----------------------------------------------------*/
void enableInterrupts(void){

	*ISER0 |= (1<<22); // ADC Interrupt Enable;
	*ISER0 |= (1<<2);   // Enable Timer1 Interrupt
}

//-- Display
void display(unsigned int decimal){
  if(decimal<10 )
  {
    *FIO2PIN = dec_to_7seg_cc[decimal];
  }
}

/*--------------------------------------------------
          SUBRUTINAS
----------------------------------------------------*/
void ADC_IRQHandler(void){
	unsigned short volatile ADC0Value0 = 0;
	unsigned short volatile ADC0Value1 = 0;
	//volatile float volts=0;
	unsigned short volatile volts=0;

	if ((*AD0STAT&1)==1) // AD0.0 is Done!
	{
		ADC0Value0=(*AD0DR0>>4)&0xFFF; // *AD0DR0 contains the most recent conversion
		if(ADC0Value0<2047)
			*FIO0SET|=(1<<22); // Vadc < Vref/2 -> Prendo Led22
		else
			*FIO0CLR|=(1<<22); // Vadc > Vref/2 -> Apago Led22
	}
	if(((*AD0STAT>>1)&1)==1) // AD0.1 is Done!
	{
		ADC0Value1=(*AD0DR1>>4)&0xFFF; // *AD0DR0 contains the most recent conversion
		volts= ADC0Value1/ ((int)DIVISOR_ADC ) ;

		unidad= volts/10 ;
		//  unidades con redondeo
		decima = volts % 10;
		if(unidad*10+decima+0.5 < volts)
			decima+=1;
	}

}

void TIMER1_IRQHandler(void){
	static unsigned short int EnableUnidad=0;

	if(EnableUnidad){
		*FIO2CLR|= (1<<Port2_Pin(8));
		display(decima);
		//*FIO2PIN = dec_to_7seg_cc[unidad];
		*FIO2SET|= (1<<Port2_Pin(7));
		EnableUnidad=!EnableUnidad;
	}
	else{
		*FIO2CLR|= (1<<Port2_Pin(7));
		display(unidad);
		//*FIO2PIN = dec_to_7seg_cc[decena];
		*FIO2SET|= (1<<Port2_Pin(8));
		EnableUnidad=!EnableUnidad;
	}
	*T1IR|=1;  //MR0 interrupt flag for match channel 0
}
/****************************************************
          MAIN
*****************************************************/
int main (void){

	config_ADC();
	config_GPIO();
	config_TIMER1();
	enableInterrupts();
	while(1){

	}

	return 0;
}
