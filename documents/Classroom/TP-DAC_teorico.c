/*
 ===============================================================================
 Name : accesoRegistros.c
 Author : $(author)
 Version :
 Copyright : $(copyright)
 Description : main definition
 ===============================================================================
 */

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

// DAC
unsigned int volatile * const DACR = (unsigned int*) 0x4008C000;
unsigned int volatile * const DACCTRL = (unsigned int*) 0x4008C004;
unsigned int volatile * const DACCNTVAL = (unsigned int*) 0x4008C008;
unsigned int volatile * const PCLKSEL0 = (unsigned int*) 0x400FC1A8;

// TMR0
unsigned int volatile * const T0IR = (unsigned int*) 0x40004000;
unsigned int volatile * const T0TCR = (unsigned int*) 0x40004004;
unsigned int volatile * const T0PR = (unsigned int*) 0x4000400C;
unsigned int volatile * const T0MCR = (unsigned int*) 0x40004014;
unsigned int volatile * const T0MR0 = (unsigned int*) 0x40004018;
unsigned int volatile * const T0MR1 = (unsigned int*) 0x4000401C;
unsigned int volatile * const T0CTCR = (unsigned int*) 0x40004070;
unsigned int volatile * const T0TC = (unsigned int*) 0x40004008;

// NVIC
unsigned int volatile * const ISER0 = (unsigned int*) 0xE000E100;
unsigned int volatile * const ICER0 = (unsigned int*) 0xE000E180;

// PIN CONNECT
unsigned int volatile * const PINSEL1 = (unsigned int*) 0x4002C004;
unsigned int volatile * const PINMODE1 = (unsigned int*) 0x4002C044;

//funciones
void configTMR0();
void configNVIC();
void configDAC();
//funciones

int DAC_Value = 0;

int main(void) {
	configTMR0();
	configNVIC();
	configDAC();

	while (1) {

	}

	return 0;
}

void configDAC() {
	*PINSEL1 &= ~(1UL << 20); // pin 0.26 - funcion '10' = AOUT
	*PINSEL1 |= (1UL << 21);
	*PINMODE1 &= ~(1UL << 20); // pin 0.26 en pullup (?)
	*PINMODE1 &= ~(1UL << 21);
	*PCLKSEL0 &= ~(1UL << 22);
	*PCLKSEL0 &= ~(1UL << 23); // PCLK en 100MHz/4 = 25MHz
}

void configTMR0() {
	*T0CTCR = 0; // TMR0 en modo timer y no contador
	*T0MR0 = 250000; // Match register 0 (realiza accion en este valor)
	*T0MR1 = 500000; // Match register 1
	*T0PR = 0; // Prescaler, cuenta hasta esto para incrementar TMR0 contador
	*T0MCR |= (1UL << 0); // Interrupt on MR0
	*T0MCR &= ~(1UL << 1); // NO Reset on MR0
	*T0MCR |= (1UL << 3); // Interrupt on MR1
	*T0MCR |= (1UL << 4); // Reset on MR1
	//INTERRUPCION EN NVIC
	*T0TCR |= (1UL << 0); // Counter Enable
	*T0TCR |= (1UL << 1); // Counter reset
	*T0TCR &= ~(1UL << 1); // Counter reset

	*T0IR &= (1UL << 0); // bajamos la bandera de interrupcion
	*T0IR &= (1UL << 1); // bajamos la bandera de interrupcion
}

void configNVIC() {
	*ISER0 |= (1UL << 1); // TMR0
}

void TIMER0_IRQHandler(void) {
	if (*T0IR & (1 << 0)) {
		*T0IR &= (1UL << 0); // bajamos la bandera de interrupcion
	} else if (*T0IR & (1 << 1)) {
		*T0IR &= (1UL << 1); // bajamos la bandera de interrupcion
	}

	*DACR = (DAC_Value & 0x3FF/*and 10 bits*/) << 6; // escribir el valor al DAC - lo convierte y saca por salida solo
	DAC_Value++;
}
